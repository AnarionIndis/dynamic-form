import React, { Component } from 'react';
import DynamicForm from './components/test-component/DynamicForm'
// import logo from './logo.svg';
import './theme/App.css';
import './theme/default.css';

class App extends Component {
  state = {
    data:[
      {id:1, name:"a", age:29, qualification:"b.com", rating:3, skills:"angular.js"},
      {id:2, name:"a", age:29, qualification:"B.Sc", rating:3, skills:"angular.js" },
      {id:3, name:"a", age:29, qualification:"B.E", rating:3, skills:"vue.js" },
      {id:4, name:"a", age:29, qualification:"b.com", rating:3, skills:"react.js" }      
    ]
  }
  render() {
    return (
      <div className="App">
          <DynamicForm className="form-for-pdf"
              title = "Registration"
              model = {
                this.formInputs()
              }
              state= {this.state}
              onSubmit = {(model) => {this.onSubmit(model)}}
          />
          {/* <pre>
            {JSON.stringify(this.state.data)}
          </pre> */}
      </div>
    );
  }

  onSubmit = (model) => {
    alert(JSON.stringify(model));
    this.setState({
      data: [model, ...this.state.data]
    })
  }

  // formInputs() {
  //   return [
  //     {key: "name", label: "Name", props: {required: true} }, //props: {required: true}
  //     {key: "age",label: "Age", type: "number"},
  //     {key: "rating",label: "Rating", type: "number", props:{min:0,max:5}},
  //     {key: "gender",label: "Gender", type:"radio",options:[
  //       {key:"male",label:"Male",name:"gender",value:"male"},
  //       {key:"female",label:"Female",name: "gender",value:"female"}
  //     ]},
  //     {key: "qualification",label: "Qualification"},
  //     {key: "city",label:"City", type:"select", value: "istanbul", options: [
  //         {key:"istanbul",label:"istanbul",value:"İstanbul"},
  //         {key:"ankara",label:"Ankara",value:"Ankara"},
  //         {key:"izmir",label:"İzmir",value:"İzmir"},
  //     ]},
  //     {key: "character",label:"character", type:"select", value: "Buck", options: [
  //       {key:"Buck",label:"Buck",value:"Buck"},
  //       {key:"Sledge",label:"Sledge",value:"Sledge"},
  //       {key:"IQ",label:"IQ",value:"IQ"},
  //   ]},
  //     {key: "skills",label:"Skills", type:"checkbox", options: [
  //         {key:"reactjs",label:"ReactJS",value:"reactjs"},
  //         {key:"angular",label:"Angular",value:"angular"},
  //         {key:"vuejs",label:"VueJS",value:"vuejs"},
  //     ]},
  //     {key: "choices",label:"Choise", type:"checkbox", options: [
  //       {key:"x",label:"x",value:"x"},
  //       {key:"y",label:"y",value:"y"},
  //       {key:"z",label:"z",value:"z"},
  //   ]},
  //   ];
  // }
  formInputs(){
    return [
      {
         "key":"Label",
         "label":"Label",
         "type":"text",
         "props":{
            "type":"text",
            "id":"item1_text_1",
            "maxlenght":"254",
            "placeholder":"",
            "autocomplete":"off",
            "data_hint":"",
            "name":"text1"
         },
         "options":[
   
         ]
      },
      {
         "key":"Number",
         "label":"Number",
         "type":"number",
         "props":{
            "type":"number",
            "id":"item2_number_1",
            "autocomplete":"off",
            "min":"0",
            "max":"999999999",
            "step":"1",
            "data_hint":"",
            "name":"number2"
         },
         "options":[
   
         ]
      },
      {
         "key":"Comments",
         "label":"Comments",
         "type":"textarea",
         "props":{
            "id":"item3_textarea_1",
            "maxlenght":"10000",
            "placeholder":"",
            "data_hint":"",
            "name":"textarea3"
         },
         "options":[
   
         ]
      },
      {
         "key":"Select an option",
         "label":"Select an option",
         "type":"radio",
         "options":[
            {
               "key":"item4_0_radio",
               "label":"Radio 1",
               "value":"Radio 1"
            },
            {
               "key":"item4_1_radio",
               "label":"Radio 2",
               "value":"Radio 2"
            },
            {
               "key":"item4_2_radio",
               "label":"Radio 3",
               "value":"Radio 3"
            }
         ]
      },
      {
         "key":"Select an option",
         "label":"Select an option",
         "type":"dropdown",
         "props":{
            "className":"item5_select_1"
         },
         "options":[
            {
               "key":"item5_0_option",
               "label":"Option 1",
               "value":"Option 1"
            },
            {
               "key":"item5_1_option",
               "label":"Option 2",
               "value":"Option 2"
            },
            {
               "key":"item5_2_option",
               "label":"Option 3",
               "value":"Option 3"
            }
         ]
      },
      {
         "key":"Check options",
         "label":"Check options",
         "type":"checkbox",
         "props":{
            "className":"fb-checkbox"
         },
         "options":[
            {
               "key":"item6_0_checkbox",
               "label":"Check 1",
               "value":"Check 1"
            },
            {
               "key":"item6_1_checkbox",
               "label":"Check 2",
               "value":"Check 2"
            },
            {
               "key":"item6_2_checkbox",
               "label":"Check 3",
               "value":"Check 3"
            }
         ]
      },
      {
         "key":"Email",
         "label":"Email",
         "type":"email",
         "props":{
            "type":"email",
            "id":"item7_email_1",
            "maxlenght":"254",
            "placeholder":"you@domain.com",
            "autocomplete":"off",
            "data_hint":"",
            "name":"email7"
         },
         "options":[
   
         ]
      }
   ]
  }
}

export default App;
