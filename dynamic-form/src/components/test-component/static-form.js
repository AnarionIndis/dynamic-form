import React, {Component} from 'react';
import html2canvas from 'html2canvas';
// import jsPDF from 'jspdf';
import html2pdf from 'html2pdf.js';
import axios from 'axios';
// import nodemailer from 'nodemailer';


class StaticForm extends Component{
    state = {
        imgData: ''
    };

    onSubmit (e){
        e.preventDefault();
        this.printDocument();
    }

    printDocument = () => {
        let form = document.getElementById('docContainer');
        html2canvas(form).then((canvas) => {
            let imgData = canvas.toDataURL('image/png');
            this.sendMail(imgData);
            console.log(imgData);
        });
        var worker = html2pdf().from(form).save();
      }

    sendMail = (canvas) =>{
        const pdfMail = axios.post('api/email', {
            imgData: canvas
        })
    }

    render(){
        return(
              <form className="fb-100-item-column fb-toplabel form-top selected-object" id="docContainer"  data-form="preview" data-percentage="100" data-boxshadow="true" onSubmit={(e)=>{this.onSubmit(e)}}>
                  <div className="fb-form-header" id="fb-form-header1">
                      <a className="fb-link-logo" id="fb-link-logo1" style={{max_width: '104px'}} target="_blank">
                          <img title="Alternative text" className="fb-logo" id="fb-logo1" style={{width: '100%', display: 'none'}} alt="Alternative text" />
                      </a>
                  </div>
                  <div className="section" id="section1">
                      <div className="column ui-sortable" id="column1">
                          <div className="fb-item" id="item172" style={{padding: '0px', max_width: '1413px', opacity: '1'}}>
                              <a className="fb-image" target="_blank">
                                  <img title="" id="item172_img_0" style={{width: '100%', height:'auto'}} alt="" src="common/images/kocsistem_logo.png" />
                              </a>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item39" style={{opacity: 1}}>
                              <div className="fb-header fb-item-alignment-center">
                                  <h2 style={{display: 'inline'}}> Genel Bilgiler</h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item4" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item4_label_0"style={{display: 'inline'}}>Hizmet Tipi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="HizmetTipi" id="item4_select_1" required="" data-hint="AH : Sunucunun tüm yönetimi Koçsistem tarafından yapılır.&#10;          CL-BCK : Sadece barındırma ve yedekleme&#10;          CL : Sadece barındırma hizmeti">
                                      <option id="item4_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item4_1_option" value="AH">
                                          AH
                                      </option>
                                      <option id="item4_2_option" value="CL-BCK">
                                          CL-BCK
                                      </option>
                                      <option id="item4_3_option" value="CL">
                                          CL
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" style={{color: 'rgb(240, 9, 63)', font_style: 'normal', font_weight: 'bold'}}>
                                  AH : Sunucunun tüm yönetimi Koçsistem tarafından yapılır.
                                  <br/>> CL-BCK : Sadece barındırma ve yedekleme
                                  <br/> CL : Sadece barındırma hizmeti
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item6" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item6_label_0"style={{display: 'inline'}}>Sunucu Tipi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="SunucuTipi" id="item6_select_1" required="" data-hint="">
                                      <option id="item6_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item6_1_option" value="Sanal">
                                          Sanal
                                      </option>
                                      <option id="item6_2_option" value="Fiziksel">
                                          Fiziksel
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item7">
                              <div className="fb-grouplabel">
                                  <label id="item7_label_0"style={{display: 'inline'}}>İşletim Sistemi Platformu</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="letimSistemiPlatformu" id="item7_select_1" required="" data-hint="">
                                      <option id="item7_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item7_1_option" value="Microsoft">
                                          Microsoft
                                      </option>
                                      <option id="item7_2_option" value="Linux">
                                          Linux
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item8" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item8_label_0"style={{display: 'inline'}}>Sanal Microsoft İşletim Sistemi Versiyonu *</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="SanalMicrosoftletimSistemiVersiyonu" id="item8_select_1" required="" data-hint="">
                                      <option id="item8_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item8_1_option" value="Windows Server 2008">
                                          Windows Server 2008
                                      </option>
                                      <option id="item8_2_option" value="Windows Server 2008 R2">
                                          Windows Server 2008 R2
                                      </option>
                                      <option id="item8_3_option" value="Windows Server 2012">
                                          Windows Server 2012
                                      </option>
                                      <option id="item8_4_option" value="Windows Server 2012 R2">
                                          Windows Server 2012 R2
                                      </option>
                                      <option id="item8_5_option" value="Windows Server 2016">
                                          Windows Server 2016
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item9" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item9_label_0"style={{display: 'inline'}}>Fiziksel Microsoft İşletim Sistemi Versiyonu</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="FizikselMicrosoftletimSistemiVersiyonu" id="item9_select_1" required="" data-hint="">
                                      <option id="item9_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item9_1_option" value="Windows Server 2008">
                                          Windows Server 2008
                                      </option>
                                      <option id="item9_2_option" value="Windows Server 2008 R2">
                                          Windows Server 2008 R2
                                      </option>
                                      <option id="item9_3_option" value="Windows Server 2012">
                                          Windows Server 2012
                                      </option>
                                      <option id="item9_4_option" value="Windows Server 2012 R2">
                                          Windows Server 2012 R2
                                      </option>
                                      <option id="item9_5_option" value="Windows Server 2016">
                                          Windows Server 2016
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item10" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item10_label_0"style={{display: 'inline'}}>İşletim Sistemi Sürüm Bilgileri</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="letimSistemiSrmBilgileri" id="item10_select_1" required="" data-hint="">
                                      <option id="item10_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item10_1_option" value="Standart Edition">
                                          Standart Edition
                                      </option>
                                      <option id="item10_2_option" value="Enterprise Edition">
                                          Enterprise Edition
                                      </option>
                                      <option id="item10_3_option" value="Datacenter Edition">
                                          Datacenter Edition
                                      </option>
                                      <option id="item10_4_option" value="Web Edition">
                                          Web Edition
                                      </option>
                                      <option id="item10_5_option" value="Itanium Edition">
                                          Itanium Edition
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item11" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item11_label_0"style={{display: 'inline'}}>Sanal Unix İşletim Sistemi Versiyonu</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="SanalUnixletimSistemiVersiyonu" id="item11_select_1" required="" data-hint="">
                                      <option id="item11_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item11_1_option" value="Redhat 6.6">
                                          Redhat 6.6
                                      </option>
                                      <option id="item11_2_option" value="Redhat 6.7">
                                          Redhat 6.7
                                      </option>
                                      <option id="item11_3_option" value="Redhat 6.8">
                                          Redhat 6.8
                                      </option>
                                      <option id="item11_4_option" value="Redhat 6.9">
                                          Redhat 6.9
                                      </option>
                                      <option id="item11_5_option" value="Redhat 7.0">
                                          Redhat 7.0
                                      </option>
                                      <option id="item11_6_option" value="Redhat 7.1">
                                          Redhat 7.1
                                      </option>
                                      <option id="item11_7_option" value="Redhat 7.2">
                                          Redhat 7.2
                                      </option>
                                      <option id="item11_8_option" value="Redhat 7.3">
                                          Redhat 7.3
                                      </option>
                                      <option id="item11_9_option" value="Redhat 7.4">
                                          Redhat 7.4
                                      </option>
                                      <option id="item11_10_option" value="Oracle Linux 6.6">
                                          Oracle Linux 6.6
                                      </option>
                                      <option id="item11_11_option" value="Oracle Linux 6.7">
                                          Oracle Linux 6.7
                                      </option>
                                      <option id="item11_12_option" value="Oracle Linux 6.8">
                                          Oracle Linux 6.8
                                      </option>
                                      <option id="item11_13_option" value="Oracle Linux 6.9">
                                          Oracle Linux 6.9
                                      </option>
                                      <option id="item11_14_option" value="Oracle Linux 7.0">
                                          Oracle Linux 7.0
                                      </option>
                                      <option id="item11_15_option" value="Oracle Linux 7.1">
                                          Oracle Linux 7.1
                                      </option>
                                      <option id="item11_16_option" value="Oracle Linux 7.2">
                                          Oracle Linux 7.2
                                      </option>
                                      <option id="item11_17_option" value="Oracle Linux 7.3">
                                          Oracle Linux 7.3
                                      </option>
                                      <option id="item11_18_option" value="Centos 6.6">
                                          Centos 6.6
                                      </option>
                                      <option id="item11_19_option" value="Centos 6.7">
                                          Centos 6.7
                                      </option>
                                      <option id="item11_20_option" value="Centos 6.8">
                                          Centos 6.8
                                      </option>
                                      <option id="item11_21_option" value="Centos 6.9">
                                          Centos 6.9
                                      </option>
                                      <option id="item11_22_option" value="Centos 7.0">
                                          Centos 7.0
                                      </option>
                                      <option id="item11_23_option" value="Centos 7.1">
                                          Centos 7.1
                                      </option>
                                      <option id="item11_24_option" value="Centos 7.2">
                                          Centos 7.2
                                      </option>
                                      <option id="item11_25_option" value="Centos 7.3">
                                          Centos 7.3
                                      </option>
                                      <option id="item11_26_option" value="Centos 7.4">
                                          Centos 7.4
                                      </option>
                                      <option id="item11_27_option" value="Suse">
                                          Suse
                                      </option>
                                      <option id="item11_28_option" value="Ubuntu">
                                          Ubuntu
                                      </option>
                                      <option id="item11_29_option" value="Solaris">
                                          Solaris
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item12" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item12_label_0"style={{display: 'inline'}}>Fiziksel Unix İşletim Sistemi Versiyonu</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="FizikselUnixletimSistemiVersiyonu" id="item12_select_1" required="" data-hint="">
                                      <option id="item12_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item12_1_option" value="Redhat 6.6">
                                          Redhat 6.6
                                      </option>
                                      <option id="item12_2_option" value="Redhat 6.7">
                                          Redhat 6.7
                                      </option>
                                      <option id="item12_3_option" value="Redhat 6.8">
                                          Redhat 6.8
                                      </option>
                                      <option id="item12_4_option" value="Redhat 6.9">
                                          Redhat 6.9
                                      </option>
                                      <option id="item12_5_option" value="Redhat 7.0">
                                          Redhat 7.0
                                      </option>
                                      <option id="item12_6_option" value="Redhat 7.1">
                                          Redhat 7.1
                                      </option>
                                      <option id="item12_7_option" value="Redhat 7.2">
                                          Redhat 7.2
                                      </option>
                                      <option id="item12_8_option" value="Redhat 7.3">
                                          Redhat 7.3
                                      </option>
                                      <option id="item12_9_option" value="Redhat 7.4">
                                          Redhat 7.4
                                      </option>
                                      <option id="item12_10_option" value="Oracle Linux 6.6">
                                          Oracle Linux 6.6
                                      </option>
                                      <option id="item12_11_option" value="Oracle Linux 6.7">
                                          Oracle Linux 6.7
                                      </option>
                                      <option id="item12_12_option" value="Oracle Linux 6.8">
                                          Oracle Linux 6.8
                                      </option>
                                      <option id="item12_13_option" value="Oracle Linux 6.9">
                                          Oracle Linux 6.9
                                      </option>
                                      <option id="item12_14_option" value="Oracle Linux 7.0">
                                          Oracle Linux 7.0
                                      </option>
                                      <option id="item12_15_option" value="Oracle Linux 7.1">
                                          Oracle Linux 7.1
                                      </option>
                                      <option id="item12_16_option" value="Oracle Linux 7.2">
                                          Oracle Linux 7.2
                                      </option>
                                      <option id="item12_17_option" value="Oracle Linux 7.3">
                                          Oracle Linux 7.3
                                      </option>
                                      <option id="item12_18_option" value="Centos 6.6">
                                          Centos 6.6
                                      </option>
                                      <option id="item12_19_option" value="Centos 6.7">
                                          Centos 6.7
                                      </option>
                                      <option id="item12_20_option" value="Centos 6.8">
                                          Centos 6.8
                                      </option>
                                      <option id="item12_21_option" value="Centos 6.9">
                                          Centos 6.9
                                      </option>
                                      <option id="item12_22_option" value="Centos 7.0">
                                          Centos 7.0
                                      </option>
                                      <option id="item12_23_option" value="Centos 7.1">
                                          Centos 7.1
                                      </option>
                                      <option id="item12_24_option" value="Centos 7.2">
                                          Centos 7.2
                                      </option>
                                      <option id="item12_25_option" value="Centos 7.3">
                                          Centos 7.3
                                      </option>
                                      <option id="item12_26_option" value="Centos 7.4">
                                          Centos 7.4
                                      </option>
                                      <option id="item12_27_option" value="Suse">
                                          Suse
                                      </option>
                                      <option id="item12_28_option" value="Ubuntu">
                                          Ubuntu
                                      </option>
                                      <option id="item12_29_option" value="Solaris">
                                          Solaris
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item174" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item174_label_0"style={{display: 'inline'}}>İşletim Sistemi Dili</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select174" id="item174_select_1" required="" data-hint="">
                                      <option id="item174_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item174_1_option" value="İngilizce">
                                          İngilizce
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item13" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item13_label_0"style={{display: 'inline'}}>Zaman ve para birimi biçimi:</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="BlgeselDilAyarlar" id="item13_select_1" data-hint="Otomasyon ile işletim sistemi kurulumunda default Türkçe seçilmelidir.">
                                      <option id="item13_0_option" value="Türkçe" selected="">
                                          Türkçe
                                      </option>
                                      <option id="item13_1_option" value="İngilizce">
                                          İngilizce
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(250, 5, 5); font-style: normal; font-weight: normal;"
                              >
                                  Otomasyon ile işletim sistemi kurulumunda default Türkçe seçilmelidir.
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item173" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item173_label_0">Klavye Tipi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select173" id="item173_select_1" required="" data-hint="">
                                      <option id="item173_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item173_1_option" value="Turkish Q">
                                          Turkish Q
                                      </option>
                                      <option id="item173_2_option" value="English">
                                          English
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item21" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item21_label_0"style={{display: 'inline'}}>
                                      Microsoft Güvenlik (MS Bulletin ID li) Güncelleme Yöntemi
                                  </label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="MicrosoftGvenlikMSBulletinIDliGncellemeYntemi" id="item21_select_1" required="" data-hint="">
                                      <option id="item21_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item21_1_option" value="Koçsistem - Manage Engine">
                                          Koçsistem - Manage Engine
                                      </option>
                                      <option id="item21_2_option" value="Çağrı Bazlı">
                                          Çağrı Bazlı
                                      </option>
                                      <option id="item21_3_option" value="Müşteri Yönetiminde">
                                          Müşteri Yönetiminde
                                      </option>
                                      <option id="item21_4_option" value="Diğer.. Lütfen Belirtiniz.">
                                          Diğer.. Lütfen Belirtiniz.
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item22" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item22_label_0"style={{display: 'inline'}}>
                                      Diğer Microsoft Güvenlik (MS Bulletin ID li) Güncelleme Yöntemi
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="DierMicrosoftGvenlikMSBulletinIDliGncellemeYntemi" id="item22_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item24" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item24_label_0"style={{display: 'inline'}}>
                                      Microsoft Güvenlik (MS Bulletin ID li) Güncelleme Aralığı
                                  </label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="MicrosoftGvenlikMSBulletinIDliGncellemeAral" id="item24_select_1" required="" data-hint="">
                                      <option id="item24_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item24_1_option" value="1 Ay">
                                          1 Ay
                                      </option>
                                      <option id="item24_2_option" value="3 Ay">
                                          3 Ay
                                      </option>
                                      <option id="item24_3_option" value="Sadece raporlama">
                                          Sadece raporlama
                                      </option>
                                      <option id="item24_4_option" value="Diğer.. Lütfen Belirtiniz.">
                                          Diğer.. Lütfen Belirtiniz.
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item25">
                              <div className="fb-grouplabel">
                                  <label id="item25_label_0"style={{display: 'inline'}}>
                                      Diğer Microsoft Güvenlik (MS Bulletin ID li) Güncelleme Aralığı
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="DierMicrosoftGvenlikMSBulletinIDliGncellemeAral" id="item25_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item26" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item26_label_0"style={{display: 'inline'}}>
                                      Lütfen Bu kısıma restart zamanını yazın.(örnek her ay 2. Salı saat:22:00 )
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="LtfenBuksmarestartzamannyaznrnekheray2Salsaat2200" id="item26_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item27">
                              <div className="fb-grouplabel">
                                  <label id="item27_label_0"style={{display: 'inline'}}>Sunucu IP adresi hangi networkte olacak</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SunucuIPadresihanginetworkteolacak" id="item27_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item28" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item28_label_0"style={{display: 'inline'}}>Sunucu Adı</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="HostName" id="item28_text_1" required="" type="text" maxLength="15" placeholder="" data-hint="Sunucu adı 15 karakteri geçemez ve Türkçe karakteri içermemelidir" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(247, 10, 10); font-style: normal; font-weight: bold;"
                                  >
                                      Sunucu adı 15 karakteri geçemez ve Türkçe karakteri içermemelidir
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item29">
                              <div className="fb-grouplabel">
                                  <label id="item29_label_0"style={{display: 'inline'}}>FQDN</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="DomainNameFQDN" id="item29_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="örnek: ksnet.local" autocomplete="off" />
                                  <div className="fb-hint" 
                                //   style="color: rgb(245, 17, 17); font-style: normal; font-weight: bold;"
                                  >
                                      örnek: ksnet.local
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item149" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item149_label_0"style={{display: 'inline'}}>Sunucu Domain'de olacak mı?</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select149" id="item149_select_1" required="" data-hint="">
                                      <option id="item149_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item149_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item149_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-20-item-column fb_cond_applied" id="item50" style={{padding_bottom: '5px', opacity: '1'}}>
                              <div className="fb-grouplabel">
                                  <label id="item50_label_0"style={{display: 'inline'}}>VCPU</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="VCPU" id="item50_number_1" required="" type="number" min="1" max="32" step="1" data-hint="Lütfen istemiş olduğunuz CPU bilgisini girin" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(242, 14, 60); font-style: normal; font-weight: bold;"
                                  >
                                      Lütfen istemiş olduğunuz CPU bilgisini girin
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-20-item-column fb_cond_applied" id="item51" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item51_label_0"style={{display: 'inline'}}>MEMORY</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="MEMORY" id="item51_number_1" required="" type="number" min="1" max="128" step="1" data-hint="Lütfen istemiş olduğunuz Memory  bilgisini girin" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(245, 12, 12); font-style: normal; font-weight: bold;"
                                  >
                                      Lütfen istemiş olduğunuz Memory bilgisini girin
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item30">
                              <div className="fb-grouplabel">
                                  <label id="item30_label_0"style={{display: 'inline'}}>Disk Tier Bilgisi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="DiskTierBilgisi" id="item30_select_1" required="" data-hint="">
                                      <option id="item30_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item30_1_option" value="T1">
                                          T1
                                      </option>
                                      <option id="item30_2_option" value="T2">
                                          T2
                                      </option>
                                      <option id="item30_3_option" value="T3">
                                          T3
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item53" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item53_label_0"style={{display: 'inline'}}>MS FILESYSTEM</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="MSFILESYSTEM" id="item53_number_1" required="" type="number" min="100" max="500" step="1" data-hint="Microsoft işletim sistemine sahip sunucularda C diski için değer girilmeli. Default 100 gb." autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(245, 10, 10); font-style: normal; font-weight: bold;"
                                  >
                                      Microsoft işletim sistemine sahip sunucularda C diski için değer girilmeli. Default 100 gb.
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item153" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item153_label_0"style={{display: 'inline'}}>SQL Data Disk</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number153" id="item153_number_1" required="" type="number" min="50" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item154" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item154_label_0"style={{display: 'inline'}}>SQL Log Disk</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number154" id="item154_number_1" required="" type="number" min="50" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item54" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item54_label_0"style={{display: 'inline'}}>UNIX ROOT DISK</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="UNIXROOTDISK" id="item54_number_1" required="" type="number" min="0" max="100" step="1" data-hint="Root disk default olarak 50 GB olarak set edilir." autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(250, 18, 18); font-style: normal; font-weight: bold;"
                                  >
                                      Root disk default olarak 50 GB olarak set edilir.
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item166" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item166_label_0"style={{display: 'inline'}}>/u01</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number166" id="item166_number_1" type="number" min="0" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item171">
                              <div className="fb-grouplabel">
                                  <label id="item171_label_0"style={{display: 'inline'}}>/ u01</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number171" id="item171_number_1" required="" type="number" min="0" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item169" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item169_label_0"style={{display: 'inline'}}>/oradata</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number169" id="item169_number_1" type="number" min="0" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item170" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item170_label_0"style={{display: 'inline'}}>/oraarch</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number170" id="item170_number_1" type="number" min="0" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item31">
                              <div className="fb-grouplabel">
                                  <label id="item31_label_0"style={{display: 'inline'}}>
                                      Sunucu mail/ftp/web sunucusu mu? (5651 yasa kapsamı) (Evet ise lütfen ek bilgi veriniz)
                                  </label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Sunucumailftpwebsunucusumu5651yasakapsamEvetiseltfenekbilgiveriniz" id="item31_select_1" required="" data-hint="">
                                      <option id="item31_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item31_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item31_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item32" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item32_label_0"style={{display: 'inline'}}>Ek Bilgi</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="EkBilgi" id="item32_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item33">
                              <div className="fb-grouplabel">
                                  <label id="item33_label_0"style={{display: 'inline'}}>Sunucu internete açık olacak mı?</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Sunucuinterneteakolacakm" id="item33_select_1" required="" data-hint="">
                                      <option id="item33_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item33_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item33_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item35" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item35_label_0"style={{display: 'inline'}}>Environment</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select35" id="item35_select_1" required="" data-hint="">
                                      <option id="item35_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item35_1_option" value="Test">
                                          Test
                                      </option>
                                      <option id="item35_2_option" value="Development">
                                          Development
                                      </option>
                                      <option id="item35_3_option" value="Production">
                                          Production
                                      </option>
                                      <option id="item35_4_option" value="Failover">
                                          Failover
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item34">
                              <div className="fb-grouplabel">
                                  <label id="item34_label_0"style={{display: 'inline'}}>Function</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Function" id="item34_select_1" required="" data-hint="">
                                      <option id="item34_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item34_1_option" value="Demo Account">
                                          Demo Account
                                      </option>
                                      <option id="item34_2_option" value="Database Server">
                                          Database Server
                                      </option>
                                      <option id="item34_3_option" value="Application Server">
                                          Application Server
                                      </option>
                                      <option id="item34_4_option" value="App &amp; DB Server">
                                          App &amp; DB Server
                                      </option>
                                      <option id="item34_5_option" value="QA Server">
                                          QA Server
                                      </option>
                                      <option id="item34_6_option" value="Test Server">
                                          Test Server
                                      </option>
                                      <option id="item34_7_option" value="Development Server">
                                          Development Server
                                      </option>
                                      <option id="item34_8_option" value="Unicenter Server">
                                          Unicenter Server
                                      </option>
                                      <option id="item34_9_option" value="Console Server">
                                          Console Server
                                      </option>
                                      <option id="item34_10_option" value="Domain Controller">
                                          Domain Controller
                                      </option>
                                      <option id="item34_11_option" value="Terminal Server">
                                          Terminal Server
                                      </option>
                                      <option id="item34_12_option" value="Print Server">
                                          Print Server
                                      </option>
                                      <option id="item34_13_option" value="Mail Server">
                                          Mail Server
                                      </option>
                                      <option id="item34_14_option" value="Proxy Server">
                                          Proxy Server
                                      </option>
                                      <option id="item34_15_option" value="Backup Server">
                                          Backup Server
                                      </option>
                                      <option id="item34_16_option" value="File Server">
                                          File Server
                                      </option>
                                      <option id="item34_17_option" value="Fax Server">
                                          Fax Server
                                      </option>
                                      <option id="item34_18_option" value="Security Server">
                                          Security Server
                                      </option>
                                      <option id="item34_19_option" value="Storage Server">
                                          Storage Server
                                      </option>
                                      <option id="item34_20_option" value="Nfs Server">
                                          Nfs Server
                                      </option>
                                      <option id="item34_21_option" value="Yedek Server">
                                          Yedek Server
                                      </option>
                                      <option id="item34_22_option" value="Web Server">
                                          Web Server
                                      </option>
                                      <option id="item34_23_option" value="Portal Server">
                                          Portal Server
                                      </option>
                                      <option id="item34_24_option" value="Ftp Server">
                                          Ftp Server
                                      </option>
                                      <option id="item34_25_option" value="OWA Server">
                                          OWA Server
                                      </option>
                                      <option id="item34_26_option" value="Dr Server">
                                          Dr Server
                                      </option>
                                      <option id="item34_27_option" value="Ids Server">
                                          Ids Server
                                      </option>
                                      <option id="item34_28_option" value="SAN Switch Server">
                                          SAN Switch Server
                                      </option>
                                      <option id="item34_29_option" value="Iris Access Server">
                                          Iris Access Server
                                      </option>
                                      <option id="item34_30_option" value="EMC Controller Server">
                                          EMC Controller Server
                                      </option>
                                      <option id="item34_31_option" value="Firewall Server">
                                          Firewall Server
                                      </option>
                                      <option id="item34_32_option" value="ESX Server">
                                          ESX Server
                                      </option>
                                      <option id="item34_33_option" value="Modem Rack">
                                          Modem Rack
                                      </option>
                                      <option id="item34_34_option" value="Virtual Host Server">
                                          Virtual Host Server
                                      </option>
                                      <option id="item34_35_option" value="Blade Enclosure">
                                          Blade Enclosure
                                      </option>
                                      <option id="item34_36_option" value="SAP Production Server">
                                          SAP Production Server
                                      </option>
                                      <option id="item34_37_option" value="SAP Test Server">
                                          SAP Test Server
                                      </option>
                                      <option id="item34_38_option" value="SAP Application Server">
                                          SAP Application Server
                                      </option>
                                      <option id="item34_39_option" value="Að Yönetim Sunucusu">
                                          Að Yönetim Sunucusu
                                      </option>
                                      <option id="item34_40_option" value="Virtual Switch">
                                          Virtual Switch
                                      </option>
                                      <option id="item34_41_option" value="Virtual Center Server">
                                          Virtual Center Server
                                      </option>
                                      <option id="item34_42_option" value="SAP DR">
                                          SAP DR
                                      </option>
                                      <option id="item34_43_option" value="Enterprise Vault Server">
                                          Enterprise Vault Server
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item210">
                              <div className="fb-grouplabel">
                                  <label id="item210_label_0"style={{display: 'inline'}}>
                                      Kaç Kullanıcı için terminal server lisansı alınacak
                                  </label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="kakullanciinlisasalnacak" id="item210_number_1" required="" type="number" min="3" max="999999999" step="1" data-hint="terminal server lisansı 3 minimum 3 kullanıcı için alınmaktadır." autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(245, 10, 10); font-style: normal; font-weight: normal;"
                                  >
                                      terminal server lisansı 3 minimum 3 kullanıcı için alınmaktadır.
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item211" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item211_label_0"style={{display: 'inline'}}>DHCP rolü kurulacak mı?</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select211" id="item211_select_1" required="" data-hint="">
                                      <option id="item211_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item211_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item211_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item212" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item212_label_0"style={{display: 'inline'}}>IIS Version</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="IISVersion" id="item212_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="Aksini belirtmediğiniz sürece en son versiyon IIS kurulmaktadır." autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(242, 9, 9); font-style: normal; font-weight: normal;"
                                  >
                                      Aksini belirtmediğiniz sürece en son versiyon IIS kurulmaktadır.
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item213" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item213_label_0"style={{display: 'inline'}}>Exchange Version bilgisi</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text213" id="item213_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="Aksini belirtmediğiniz sürece en son versiyon Exchange Server kurulmaktadır." autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(247, 13, 13); font-style: normal; font-weight: normal;"
                                  >
                                      Aksini belirtmediğiniz sürece en son versiyon Exchange Server kurulmaktadır.
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item36" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item36_label_0"style={{display: 'inline'}}>Sunucu Kontak İsim-Soyisim</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SunucuKontaksim-Soyisim" id="item36_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb-side-by-side fb_cond_applied" id="item37" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item37_label_0"style={{display: 'inline'}}>Hizmetler</label>
                              </div>
                              <div className="fb-checkbox" style={{font_size: '18px'}}>
                                  <label id="item37_0_label">
                                      <input name="hizmetler[]" id="item37_0_checkbox" type="checkbox" value="Veritabanı Yönetimi" data-hint=""/>
                                      <span className="fb-fieldlabel" id="item37_0_span">
                                                    Veritabanı
                                                    Yönetimi
                                                </span>
                                  </label>
                                  <label id="item37_1_label">
                                      <input name="hizmetler[]" id="item37_1_checkbox" type="checkbox" value="Veri Depolama Yönetimi"/>
                                      <span className="fb-fieldlabel" id="item37_1_span">Veri Depolama Yönetimi</span>
                                  </label>
                                  <label id="item37_2_label">
                                      <input name="hizmetler[]" id="item37_2_checkbox" type="checkbox" value="SAP Yönetimi"/>
                                      <span className="fb-fieldlabel" id="item37_2_span">
                                                    SAP
                                                    Yönetimi
                                                </span>
                                  </label>
                                  <label id="item37_3_label">
                                      <input name="hizmetler[]" id="item37_3_checkbox" type="checkbox" value="Veri Yedekleme Yönetimi"/>
                                      <span className="fb-fieldlabel" id="item37_3_span">Veri Yedekleme Yönetimi</span>
                                  </label>
                                  <label id="item37_4_label">
                                      <input name="hizmetler[]" id="item37_4_checkbox" type="checkbox" value="İzleme"/>
                                      <span className="fb-fieldlabel" id="item37_4_span">İzleme</span>
                                  </label>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb-side-by-side fb_cond_applied" id="item42" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item42_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-checkbox" style={{font_size: '18px'}}>
                                  <label id="item42_0_label">
                                      <input name="hizmetler2[]" id="item42_0_checkbox" type="checkbox" value="Veri Yedekleme Yönetimi"/>
                                      <span className="fb-fieldlabel" id="item42_0_span">Veri Yedekleme Yönetimi</span>
                                  </label>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item60" style={{opacity: 1}}>
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>            Veritabanı Yönetimi           </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item61" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item61_label_0"style={{display: 'inline'}}>Veritabanı Çeşidi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Veritabaneidi" id="item61_select_1" required="" data-hint="">
                                      <option id="item61_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item61_1_option" value="Microsoft_SQL">
                                          Microsoft_SQL
                                      </option>
                                      <option id="item61_2_option" value="Oracle">
                                          Oracle
                                      </option>
                                      <option id="item61_3_option" value="Mysql">
                                          Mysql
                                      </option>
                                      <option id="item61_4_option" value="IBM_DB2">
                                          IBM_DB2
                                      </option>
                                      <option id="item61_5_option" value="SYBASE">
                                          SYBASE
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item62" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item62_label_0"style={{display: 'inline'}}>SQL Ürün adı</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="SQLrnad" id="item62_select_1" required="" data-hint="">
                                      <option id="item62_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item62_1_option" value="SQL Server 2008 (Express) (x32)">
                                          SQL Server 2008 (Express) (x32)
                                      </option>
                                      <option id="item62_2_option" value="SQL Server 2008 (Express) (x64)">
                                          SQL Server 2008 (Express) (x64)
                                      </option>
                                      <option id="item62_3_option" value="SQL Server 2008 (Standart)(x32)">
                                          SQL Server 2008 (Standart)(x32)
                                      </option>
                                      <option id="item62_4_option" value="SQL Server 2008 (Standart)(x64)">
                                          SQL Server 2008 (Standart)(x64)
                                      </option>
                                      <option id="item62_5_option" value="SQL Server 2008 (Enterprise)(x32)">
                                          SQL Server 2008 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_6_option" value="SQL Server 2008 (Enterprise)(x64)">
                                          SQL Server 2008 (Enterprise)(x64)
                                      </option>
                                      <option id="item62_7_option" value="SQL Server 2008 R2 (Express)(x32)">
                                          SQL Server 2008 R2 (Express)(x32)
                                      </option>
                                      <option id="item62_8_option" value="SQL Server 2008 R2 (Express)(x64)">
                                          SQL Server 2008 R2 (Express)(x64)
                                      </option>
                                      <option id="item62_9_option" value="SQL Server 2008 R2 (Standart)(x32)">
                                          SQL Server 2008 R2 (Standart)(x32)
                                      </option>
                                      <option id="item62_10_option" value="SQL Server 2008 R2 (Standart)(x64)">
                                          SQL Server 2008 R2 (Standart)(x64)
                                      </option>
                                      <option id="item62_11_option" value="SQL Server 2008 R2 (Enterprise)(x32)">
                                          SQL Server 2008 R2 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_12_option" value="SQL Server 2008 R2 (Enterprise)(x64)">
                                          SQL Server 2008 R2 (Enterprise)(x64)
                                      </option>
                                      <option id="item62_13_option" value="SQL Server 2012 (Express)(x32)">
                                          SQL Server 2012 (Express)(x32)
                                      </option>
                                      <option id="item62_14_option" value="SQL Server 2012 (Express)(x64)">
                                          SQL Server 2012 (Express)(x64)
                                      </option>
                                      <option id="item62_15_option" value="SQL Server 2012 (Standart)(x32)">
                                          SQL Server 2012 (Standart)(x32)
                                      </option>
                                      <option id="item62_16_option" value="SQL Server 2012 (Standart)(x64)">
                                          SQL Server 2012 (Standart)(x64)
                                      </option>
                                      <option id="item62_17_option" value="SQL Server 2012 Business Intelligence (x32)">
                                          SQL Server 2012 Business Intelligence (x32)
                                      </option>
                                      <option id="item62_18_option" value="SQL Server 2012 Business Intelligence (x64)">
                                          SQL Server 2012 Business Intelligence (x64)
                                      </option>
                                      <option id="item62_19_option" value="SQL Server 2012 (Enterprise)(x32)">
                                          SQL Server 2012 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_20_option" value="SQL Server 2012 (Enterprise)(x64)">
                                          SQL Server 2012 (Enterprise)(x64)
                                      </option>
                                      <option id="item62_21_option" value="SQL Server 2014 (Express)(x32)">
                                          SQL Server 2014 (Express)(x32)
                                      </option>
                                      <option id="item62_22_option" value="SQL Server 2014 (Express)(x64)">
                                          SQL Server 2014 (Express)(x64)
                                      </option>
                                      <option id="item62_23_option" value="SQL Server 2014 (Standart)(x32)">
                                          SQL Server 2014 (Standart)(x32)
                                      </option>
                                      <option id="item62_24_option" value="SQL Server 2014 (Standart)(x64)">
                                          SQL Server 2014 (Standart)(x64)
                                      </option>
                                      <option id="item62_25_option" value="SQL Server 2014 (Enterprise)(x32)">
                                          SQL Server 2014 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_26_option" value="SQL Server 2014 (Enterprise)(x64)">
                                          SQL Server 2014 (Enterprise)(x64)
                                      </option>
                                      <option id="item62_27_option" value="SQL Server 2016 (Express)(x32)">
                                          SQL Server 2016 (Express)(x32)
                                      </option>
                                      <option id="item62_28_option" value="SQL Server 2016 (Express)(x64)">
                                          SQL Server 2016 (Express)(x64)
                                      </option>
                                      <option id="item62_29_option" value="SQL Server 2016 (Standart)(x32)"> SQL Server 2016 (Standart)(x32) </option>
                                      <option id="item62_30_option" value="SQL Server 2016 (Standart)(x64)">
                                          SQL Server 2016 (Standart)(x64)
                                      </option>
                                      <option id="item62_31_option" value="SQL Server 2016 (Enterprise)(x32)">
                                          SQL Server 2016 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_32_option" value="SQL Server 2016 (Enterprise)(x64)">
                                          SQL Server 2016 (Enterprise)(x64)
                                      </option>
                                      <option id="item62_33_option" value="SQL Server 2017 (Enterprise)(x32)">
                                          SQL Server 2017 (Enterprise)(x32)
                                      </option>
                                      <option id="item62_34_option" value="SQL Server 2017 (Enterprise)(x64)">
                                          SQL Server 2017 (Enterprise)(x64)
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item155" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item155_label_0"style={{display: 'inline'}}>Service Pack ( SP ) ve Cumulative Update ( Cu)</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text155" id="item155_text_1" type="text" maxLength="254" placeholder="" data-hint="Aksi Belirtilmediği süreci en son versiyon SP ve CU ile yüklenir" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(240, 17, 17); font-style: normal; font-weight: normal;"
                                  >
                                      Aksi Belirtilmediği süreci en son versiyon SP ve CU ile yüklenir
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item156">
                              <div className="fb-grouplabel">
                                  <label id="item156_label_0"style={{display: 'inline'}}>Lisans Tercihi ?</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select156" id="item156_select_1" required="" data-hint="">
                                      <option id="item156_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item156_1_option" value="Koçsistem Lisansı">
                                          Koçsistem Lisansı
                                      </option>
                                      <option id="item156_2_option" value="Müşteri Lİsansı">
                                          Müşteri Lİsansı
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item63">
                              <div className="fb-grouplabel">
                                  <label id="item63_label_0"style={{display: 'inline'}}>Oracle Ürün Adı</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="OraclernAd" id="item63_select_1" required="" data-hint="">
                                      <option id="item63_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item63_1_option" value="Oracle Enterprise 10g (x32)">
                                          Oracle Enterprise 10g (x32)
                                      </option>
                                      <option id="item63_2_option" value="Oracle Enterprise 10g (x64)">
                                          Oracle Enterprise 10g (x64)
                                      </option>
                                      <option id="item63_3_option" value="Oracle Standart 10g (x32)">
                                          Oracle Standart 10g (x32)
                                      </option>
                                      <option id="item63_4_option" value="Oracle Standart 10g (x64)">
                                          Oracle Standart 10g (x64)
                                      </option>
                                      <option id="item63_5_option" value="Oracle Enterprise 11g (x32)">
                                          Oracle Enterprise 11g (x32)
                                      </option>
                                      <option id="item63_6_option" value="Oracle Enterprise 11g (x64)">
                                          Oracle Enterprise 11g (x64)
                                      </option>
                                      <option id="item63_7_option" value="Oracle Standart 11g (x32)">
                                          Oracle Standart 11g (x32)
                                      </option>
                                      <option id="item63_8_option" value="Oracle Standart 11g (x64)">
                                          Oracle Standart 11g (x64)
                                      </option>
                                      <option id="item63_9_option" value="Oracle Standart 12c (x32)">
                                          Oracle Standart 12c (x32)
                                      </option>
                                      <option id="item63_10_option" value="Oracle Standart 12c (x64)">
                                          Oracle Standart 12c (x64)
                                      </option>
                                      <option id="item63_11_option" value="Oracle Enterprise 12c(x32)">
                                          Oracle Enterprise 12c(x32)
                                      </option>
                                      <option id="item63_12_option" value="Oracle Enterprise 12c(x64)">
                                          Oracle Enterprise 12c(x64)
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item64" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item64_label_0"style={{display: 'inline'}}>Mysql ürün adı</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Mysqlrnad" id="item64_select_1" required="" data-hint="">
                                      <option id="item64_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item64_1_option" value="MySQL Community (x32)">
                                          MySQL Community (x32)
                                      </option>
                                      <option id="item64_2_option" value="MySQL Community (x64)">
                                          MySQL Community (x64)
                                      </option>
                                      <option id="item64_3_option" value="MySQL Standart Edition (x32)">
                                          MySQL Standart Edition (x32)
                                      </option>
                                      <option id="item64_4_option" value="MySQL Standart Edition (x64)">
                                          MySQL Standart Edition (x64)
                                      </option>
                                      <option id="item64_5_option" value="MySQL Enterprise Edition (x32)">
                                          MySQL Enterprise Edition (x32)
                                      </option>
                                      <option id="item64_6_option" value="MySQL Enterprise Edition (x64)">
                                          MySQL Enterprise Edition (x64)
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item65" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item65_label_0"style={{display: 'inline'}}>DB2 ürün adı</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="DB2rnad" id="item65_select_1" required="" data-hint="">
                                      <option id="item65_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item65_1_option" value="IBM DB2 Standart 9.7 (x32)">
                                          IBM DB2 Standart 9.7 (x32)
                                      </option>
                                      <option id="item65_2_option" value="IBM DB2 Standart 9.7 (x64)">
                                          IBM DB2 Standart 9.7 (x64)
                                      </option>
                                      <option id="item65_3_option" value="IBM DB2 Enterprise 9.7  (x32)">
                                          IBM DB2 Enterprise 9.7 (x32)
                                      </option>
                                      <option id="item65_4_option" value="IBM DB2 Enterprise 9.7 (x64)">
                                          IBM DB2 Enterprise 9.7 (x64)
                                      </option>
                                      <option id="item65_5_option" value="IBM DB2 Standart 10 (x32)">
                                          IBM DB2 Standart 10 (x32)
                                      </option>
                                      <option id="item65_6_option" value="IBM DB2 Standart 10 (x64)">
                                          IBM DB2 Standart 10 (x64)
                                      </option>
                                      <option id="item65_7_option" value="IBM DB2 Enterprise 10 (x32)">
                                          IBM DB2 Enterprise 10 (x32)
                                      </option>
                                      <option id="item65_8_option" value="IBM DB2 Enterprise 10 (x64)">
                                          IBM DB2 Enterprise 10 (x64)
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item66" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item66_label_0"style={{display: 'inline'}}>Sybase ürün adı</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="Sybasernad" id="item66_select_1" required="" data-hint="">
                                      <option id="item66_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item66_1_option" value="Sybase 12  Standart(x32)">
                                          Sybase 12 Standart(x32)
                                      </option>
                                      <option id="item66_2_option" value="Sybase 12  Standart(x64)">
                                          Sybase 12 Standart(x64)
                                      </option>
                                      <option id="item66_3_option" value="Sybase 12  Enterprise (x32)">
                                          Sybase 12 Enterprise (x32)
                                      </option>
                                      <option id="item66_4_option" value="Sybase 12  Enterprise (x64)">
                                          Sybase 12 Enterprise (x64)
                                      </option>
                                      <option id="item66_5_option" value="Sybase 15  Standart(x32)">
                                          Sybase 15 Standart(x32)
                                      </option>
                                      <option id="item66_6_option" value="Sybase 15  Standart(x64)">
                                          Sybase 15 Standart(x64)
                                      </option>
                                      <option id="item66_7_option" value="Sybase 15  Enterprise (x32)">
                                          Sybase 15 Enterprise (x32)
                                      </option>
                                      <option id="item66_8_option" value="Sybase 15  Enterprise (x64)">
                                          Sybase 15 Enterprise (x64)
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item67">
                              <div className="fb-grouplabel">
                                  <label id="item67_label_0"style={{display: 'inline'}}>Oracle Versiyon</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="OracleVersiyon" id="item67_select_1" required="" data-hint="">
                                      <option id="item67_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item67_1_option" value="10.1.0.2">
                                          10.1.0.2
                                      </option>
                                      <option id="item67_2_option" value="10.1.0.3">
                                          10.1.0.3
                                      </option>
                                      <option id="item67_3_option" value="10.1.0.4">
                                          10.1.0.4
                                      </option>
                                      <option id="item67_4_option" value="10.1.0.5">
                                          10.1.0.5
                                      </option>
                                      <option id="item67_5_option" value="10.2.0.1">
                                          10.2.0.1
                                      </option>
                                      <option id="item67_6_option" value="10.2.0.2">
                                          10.2.0.2
                                      </option>
                                      <option id="item67_7_option" value="10.2.0.3">
                                          10.2.0.3
                                      </option>
                                      <option id="item67_8_option" value="10.2.0.4">
                                          10.2.0.4
                                      </option>
                                      <option id="item67_9_option" value="10.2.0.5">
                                          10.2.0.5
                                      </option>
                                      <option id="item67_10_option" value="11.1.0.6">
                                          11.1.0.6
                                      </option>
                                      <option id="item67_11_option" value="11.1.0.7">
                                          11.1.0.7
                                      </option>
                                      <option id="item67_12_option" value="11.2.0.1">
                                          11.2.0.1
                                      </option>
                                      <option id="item67_13_option" value="11.2.0.2">
                                          11.2.0.2
                                      </option>
                                      <option id="item67_14_option" value="11.2.0.3">
                                          11.2.0.3
                                      </option>
                                      <option id="item67_15_option" value="11.2.0.4">
                                          11.2.0.4
                                      </option>
                                      <option id="item67_16_option" value="12.1.0.1">
                                          12.1.0.1
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item68">
                              <div className="fb-grouplabel">
                                  <label id="item68_label_0"style={{display: 'inline'}}>DB2 Versiyon</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="DB2Versiyon" id="item68_select_1" required="" data-hint="">
                                      <option id="item68_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item68_1_option" value="DB2 10.1">
                                          DB2 10.1
                                      </option>
                                      <option id="item68_2_option" value="DB2 9.8">
                                          DB2 9.8
                                      </option>
                                      <option id="item68_3_option" value="DB2 9.7">
                                          DB2 9.7
                                      </option>
                                      <option id="item68_4_option" value="DB2 9.5">
                                          DB2 9.5
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item69" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item69_label_0"style={{display: 'inline'}}>Sybase Versiyon</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="SybaseVersiyon" id="item69_select_1" required="" data-hint="">
                                      <option id="item69_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item69_1_option" value="Sybase 12.5.0">
                                          Sybase 12.5.0
                                      </option>
                                      <option id="item69_2_option" value="Sybase 12.5.1">
                                          Sybase 12.5.1
                                      </option>
                                      <option id="item69_3_option" value="Sybase 12.5.2">
                                          Sybase 12.5.2
                                      </option>
                                      <option id="item69_4_option" value="Sybase 12.5.3">
                                          Sybase 12.5.3
                                      </option>
                                      <option id="item69_5_option" value="Sybase 12.5.4">
                                          Sybase 12.5.4
                                      </option>
                                      <option id="item69_6_option" value="Sybase 15.0">
                                          Sybase 15.0
                                      </option>
                                      <option id="item69_7_option" value="Sybase 15.1">
                                          Sybase 15.1
                                      </option>
                                      <option id="item69_8_option" value="Sybase 15.2">
                                          Sybase 15.2
                                      </option>
                                      <option id="item69_9_option" value="Sybase 15.3">
                                          Sybase 15.3
                                      </option>
                                      <option id="item69_10_option" value="Sybase 15.4">
                                          Sybase 15.4
                                      </option>
                                      <option id="item69_11_option" value="Sybase 15.5">
                                          Sybase 15.5
                                      </option>
                                      <option id="item69_12_option" value="Sybase 15.6">
                                          Sybase 15.6
                                      </option>
                                      <option id="item69_13_option" value="Sybase 15.7">
                                          Sybase 15.7
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item70" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item70_label_0"style={{display: 'inline'}}>Mysql Versiyon</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select70" id="item70_select_1" required="" data-hint="">
                                      <option id="item70_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item70_1_option" value="MySQL 5.1">
                                          MySQL 5.1
                                      </option>
                                      <option id="item70_2_option" value="MySQL 5.5">
                                          MySQL 5.5
                                      </option>
                                      <option id="item70_3_option" value="MySQL 5.6">
                                          MySQL 5.6
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-three-column fb-100-item-column fb_cond_applied" id="item159" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item159_label_0"style={{display: 'inline'}}>Kurulum Opsiyonları</label>
                              </div>
                              <div className="fb-radio">
                                  <label id="item159_0_label">
                                      <input name="KurulumOpsiyonlar" id="item159_0_radio" required="" type="radio" value="Software Only"/>
                                      <span className="fb-fieldlabel" id="item159_0_span">Software Only</span>
                                  </label>
                                  <label id="item159_1_label">
                                      <input name="KurulumOpsiyonlar" id="item159_1_radio" required="" type="radio" value="Software + Instance create"/>
                                      <span className="fb-fieldlabel" id="item159_1_span">
                                                    Software + Instance
                                                    create
                                                </span>
                                  </label>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item71">
                              <div className="fb-grouplabel">
                                  <label id="item71_label_0"style={{display: 'inline'}}>Veritabanı Kurulum Tipi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="VeritabanKurulumTipi" id="item71_select_1" required="" data-hint="">
                                      <option id="item71_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item71_1_option" value="Oracle Ýþletim sistemi cluster">
                                          Oracle Ýþletim sistemi cluster
                                      </option>
                                      <option id="item71_2_option" value="Oracle RAC">
                                          Oracle RAC
                                      </option>
                                      <option id="item71_3_option" value="Stand-alone">
                                          Stand-alone
                                      </option>
                                      <option id="item71_4_option" value="SQL Server Cluster">
                                          SQL Server Cluster
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item72">
                              <div className="fb-grouplabel">
                                  <label id="item72_label_0"style={{display: 'inline'}}>Sql Collation Type</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="DilAyarlar" id="item72_select_1" required="" data-hint="">
                                      <option id="item72_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item72_1_option" value="Latin1_General_BIN">
                                          Latin1_General_BIN
                                      </option>
                                      <option id="item72_2_option" value="Latin1_General_BIN2">
                                          Latin1_General_BIN2
                                      </option>
                                      <option id="item72_3_option" value="Latin1_General_CI_AI">
                                          Latin1_General_CI_AI
                                      </option>
                                      <option id="item72_4_option" value="Latin1_General_CI_AI_WS">
                                          Latin1_General_CI_AI_WS
                                      </option>
                                      <option id="item72_5_option" value="Latin1_General_CI_AI_KS">
                                          Latin1_General_CI_AI_KS
                                      </option>
                                      <option id="item72_6_option" value="Latin1_General_CI_AI_KS_WS">
                                          Latin1_General_CI_AI_KS_WS
                                      </option>
                                      <option id="item72_7_option" value="Latin1_General_CI_AS">
                                          Latin1_General_CI_AS
                                      </option>
                                      <option id="item72_8_option" value="Latin1_General_CI_AS_WS">
                                          Latin1_General_CI_AS_WS
                                      </option>
                                      <option id="item72_9_option" value="Latin1_General_CI_AS_KS">
                                          Latin1_General_CI_AS_KS
                                      </option>
                                      <option id="item72_10_option" value="Latin1_General_CI_AS_KS_WS">
                                          Latin1_General_CI_AS_KS_WS
                                      </option>
                                      <option id="item72_11_option" value="Latin1_General_CS_AI">
                                          Latin1_General_CS_AI
                                      </option>
                                      <option id="item72_12_option" value="Latin1_General_CS_AI_WS">
                                          Latin1_General_CS_AI_WS
                                      </option>
                                      <option id="item72_13_option" value="Latin1_General_CS_AI_KS">
                                          Latin1_General_CS_AI_KS
                                      </option>
                                      <option id="item72_14_option" value="Latin1_General_CS_AI_KS_WS">
                                          Latin1_General_CS_AI_KS_WS
                                      </option>
                                      <option id="item72_15_option" value="Latin1_General_CS_AS">
                                          Latin1_General_CS_AS
                                      </option>
                                      <option id="item72_16_option" value="Latin1_General_CS_AS_WS">
                                          Latin1_General_CS_AS_WS
                                      </option>
                                      <option id="item72_17_option" value="Latin1_General_CS_AS_KS">
                                          Latin1_General_CS_AS_KS
                                      </option>
                                      <option id="item72_18_option" value="Latin1_General_CS_AS_KS_WS">
                                          Latin1_General_CS_AS_KS_WS
                                      </option>
                                      <option id="item72_19_option" value="Latin1_General_100_BIN">
                                          Latin1_General_100_BIN
                                      </option>
                                      <option id="item72_20_option" value="Latin1_General_100_BIN2">
                                          Latin1_General_100_BIN2
                                      </option>
                                      <option id="item72_21_option" value="Latin1_General_100_CI_AI">
                                          Latin1_General_100_CI_AI
                                      </option>
                                      <option id="item72_22_option" value="Latin1_General_100_CI_AI_WS">
                                          Latin1_General_100_CI_AI_WS
                                      </option>
                                      <option id="item72_23_option" value="Latin1_General_100_CI_AI_KS">
                                          Latin1_General_100_CI_AI_KS
                                      </option>
                                      <option id="item72_24_option" value="Latin1_General_100_CI_AI_KS_WS">
                                          Latin1_General_100_CI_AI_KS_WS
                                      </option>
                                      <option id="item72_25_option" value="Latin1_General_100_CI_AS">
                                          Latin1_General_100_CI_AS
                                      </option>
                                      <option id="item72_26_option" value="Latin1_General_100_CI_AS_WS">
                                          Latin1_General_100_CI_AS_WS
                                      </option>
                                      <option id="item72_27_option" value="Latin1_General_100_CI_AS_KS">
                                          Latin1_General_100_CI_AS_KS
                                      </option>
                                      <option id="item72_28_option" value="Latin1_General_100_CI_AS_KS_WS">
                                          Latin1_General_100_CI_AS_KS_WS
                                      </option>
                                      <option id="item72_29_option" value="Latin1_General_100_CS_AI">
                                          Latin1_General_100_CS_AI
                                      </option>
                                      <option id="item72_30_option" value="Latin1_General_100_CS_AI_WS">
                                          Latin1_General_100_CS_AI_WS
                                      </option>
                                      <option id="item72_31_option" value="Latin1_General_100_CS_AI_KS">
                                          Latin1_General_100_CS_AI_KS
                                      </option>
                                      <option id="item72_32_option" value="Latin1_General_100_CS_AI_KS_WS">
                                          Latin1_General_100_CS_AI_KS_WS
                                      </option>
                                      <option id="item72_33_option" value="Latin1_General_100_CS_AS">
                                          Latin1_General_100_CS_AS
                                      </option>
                                      <option id="item72_34_option" value="Latin1_General_100_CS_AS_WS">
                                          Latin1_General_100_CS_AS_WS
                                      </option>
                                      <option id="item72_35_option" value="Latin1_General_100_CS_AS_KS">
                                          Latin1_General_100_CS_AS_KS
                                      </option>
                                      <option id="item72_36_option" value="Latin1_General_100_CS_AS_KS_WS">
                                          Latin1_General_100_CS_AS_KS_WS
                                      </option>
                                      <option id="item72_37_option" value="Latin1_General_100_CI_AI_SC">
                                          Latin1_General_100_CI_AI_SC
                                      </option>
                                      <option id="item72_38_option" value="Latin1_General_100_CI_AI_WS_SC">
                                          Latin1_General_100_CI_AI_WS_SC
                                      </option>
                                      <option id="item72_39_option" value="Latin1_General_100_CI_AI_KS_SC">
                                          Latin1_General_100_CI_AI_KS_SC
                                      </option>
                                      <option id="item72_40_option" value="Latin1_General_100_CI_AI_KS_WS_SC">
                                          Latin1_General_100_CI_AI_KS_WS_SC
                                      </option>
                                      <option id="item72_41_option" value="Latin1_General_100_CI_AS_SC">
                                          Latin1_General_100_CI_AS_SC
                                      </option>
                                      <option id="item72_42_option" value="Latin1_General_100_CI_AS_WS_SC">
                                          Latin1_General_100_CI_AS_WS_SC
                                      </option>
                                      <option id="item72_43_option" value="Latin1_General_100_CI_AS_KS_SC">
                                          Latin1_General_100_CI_AS_KS_SC
                                      </option>
                                      <option id="item72_44_option" value="Latin1_General_100_CI_AS_KS_WS_SC">
                                          Latin1_General_100_CI_AS_KS_WS_SC
                                      </option>
                                      <option id="item72_45_option" value="Latin1_General_100_CS_AI_SC">
                                          Latin1_General_100_CS_AI_SC
                                      </option>
                                      <option id="item72_46_option" value="Latin1_General_100_CS_AI_WS_SC">
                                          Latin1_General_100_CS_AI_WS_SC
                                      </option>
                                      <option id="item72_47_option" value="Latin1_General_100_CS_AI_KS_SC">
                                          Latin1_General_100_CS_AI_KS_SC
                                      </option>
                                      <option id="item72_48_option" value="Latin1_General_100_CS_AI_KS_WS_SC">
                                          Latin1_General_100_CS_AI_KS_WS_SC
                                      </option>
                                      <option id="item72_49_option" value="Latin1_General_100_CS_AS_SC">
                                          Latin1_General_100_CS_AS_SC
                                      </option>
                                      <option id="item72_50_option" value="Latin1_General_100_CS_AS_WS_SC">
                                          Latin1_General_100_CS_AS_WS_SC
                                      </option>
                                      <option id="item72_51_option" value="Latin1_General_100_CS_AS_KS_SC">
                                          Latin1_General_100_CS_AS_KS_SC
                                      </option>
                                      <option id="item72_52_option" value="Latin1_General_100_CS_AS_KS_WS_SC">
                                          Latin1_General_100_CS_AS_KS_WS_SC
                                      </option>
                                      <option id="item72_53_option" value="Turkish_BIN">
                                          Turkish_BIN
                                      </option>
                                      <option id="item72_54_option" value="Turkish_BIN2">
                                          Turkish_BIN2
                                      </option>
                                      <option id="item72_55_option" value="Turkish_CI_AI">
                                          Turkish_CI_AI
                                      </option>
                                      <option id="item72_56_option" value="Turkish_CI_AI_WS">
                                          Turkish_CI_AI_WS
                                      </option>
                                      <option id="item72_57_option" value="Turkish_CI_AI_KS">
                                          Turkish_CI_AI_KS
                                      </option>
                                      <option id="item72_58_option" value="Turkish_CI_AI_KS_WS">
                                          Turkish_CI_AI_KS_WS
                                      </option>
                                      <option id="item72_59_option" value="Turkish_CI_AS">
                                          Turkish_CI_AS
                                      </option>
                                      <option id="item72_60_option" value="Turkish_CI_AS_WS">
                                          Turkish_CI_AS_WS
                                      </option>
                                      <option id="item72_61_option" value="Turkish_CI_AS_KS">
                                          Turkish_CI_AS_KS
                                      </option>
                                      <option id="item72_62_option" value="Turkish_CI_AS_KS_WS">
                                          Turkish_CI_AS_KS_WS
                                      </option>
                                      <option id="item72_63_option" value="Turkish_CS_AI">
                                          Turkish_CS_AI
                                      </option>
                                      <option id="item72_64_option" value="Turkish_CS_AI_WS">
                                          Turkish_CS_AI_WS
                                      </option>
                                      <option id="item72_65_option" value="Turkish_CS_AI_KS">
                                          Turkish_CS_AI_KS
                                      </option>
                                      <option id="item72_66_option" value="Turkish_CS_AI_KS_WS">
                                          Turkish_CS_AI_KS_WS
                                      </option>
                                      <option id="item72_67_option" value="Turkish_CS_AS">
                                          Turkish_CS_AS
                                      </option>
                                      <option id="item72_68_option" value="Turkish_CS_AS_WS">
                                          Turkish_CS_AS_WS
                                      </option>
                                      <option id="item72_69_option" value="Turkish_CS_AS_KS">
                                          Turkish_CS_AS_KS
                                      </option>
                                      <option id="item72_70_option" value="Turkish_CS_AS_KS_WS">
                                          Turkish_CS_AS_KS_WS
                                      </option>
                                      <option id="item72_71_option" value="Turkish_100_BIN">
                                          Turkish_100_BIN
                                      </option>
                                      <option id="item72_72_option" value="Turkish_100_BIN2">
                                          Turkish_100_BIN2
                                      </option>
                                      <option id="item72_73_option" value="Turkish_100_CI_AI">
                                          Turkish_100_CI_AI
                                      </option>
                                      <option id="item72_74_option" value="Turkish_100_CI_AI_WS">
                                          Turkish_100_CI_AI_WS
                                      </option>
                                      <option id="item72_75_option" value="Turkish_100_CI_AI_KS">
                                          Turkish_100_CI_AI_KS
                                      </option>
                                      <option id="item72_76_option" value="Turkish_100_CI_AI_KS_WS">
                                          Turkish_100_CI_AI_KS_WS
                                      </option>
                                      <option id="item72_77_option" value="Turkish_100_CI_AS">
                                          Turkish_100_CI_AS
                                      </option>
                                      <option id="item72_78_option" value="Turkish_100_CI_AS_WS">
                                          Turkish_100_CI_AS_WS
                                      </option>
                                      <option id="item72_79_option" value="Turkish_100_CI_AS_KS">
                                          Turkish_100_CI_AS_KS
                                      </option>
                                      <option id="item72_80_option" value="Turkish_100_CI_AS_KS_WS">
                                          Turkish_100_CI_AS_KS_WS
                                      </option>
                                      <option id="item72_81_option" value="Turkish_100_CS_AI">
                                          Turkish_100_CS_AI
                                      </option>
                                      <option id="item72_82_option" value="Turkish_100_CS_AI_WS">
                                          Turkish_100_CS_AI_WS
                                      </option>
                                      <option id="item72_83_option" value="Turkish_100_CS_AI_KS">
                                          Turkish_100_CS_AI_KS
                                      </option>
                                      <option id="item72_84_option" value="Turkish_100_CS_AI_KS_WS">
                                          Turkish_100_CS_AI_KS_WS
                                      </option>
                                      <option id="item72_85_option" value="Turkish_100_CS_AS">
                                          Turkish_100_CS_AS
                                      </option>
                                      <option id="item72_86_option" value="Turkish_100_CS_AS_WS">
                                          Turkish_100_CS_AS_WS
                                      </option>
                                      <option id="item72_87_option" value="Turkish_100_CS_AS_KS">
                                          Turkish_100_CS_AS_KS
                                      </option>
                                      <option id="item72_88_option" value="Turkish_100_CS_AS_KS_WS">
                                          Turkish_100_CS_AS_KS_WS
                                      </option>
                                      <option id="item72_89_option" value="Turkish_100_CI_AI_SC">
                                          Turkish_100_CI_AI_SC
                                      </option>
                                      <option id="item72_90_option" value="Turkish_100_CI_AI_WS_SC">
                                          Turkish_100_CI_AI_WS_SC
                                      </option>
                                      <option id="item72_91_option" value="Turkish_100_CI_AI_KS_SC">
                                          Turkish_100_CI_AI_KS_SC
                                      </option>
                                      <option id="item72_92_option" value="Turkish_100_CI_AI_KS_WS_SC">
                                          Turkish_100_CI_AI_KS_WS_SC
                                      </option>
                                      <option id="item72_93_option" value="Turkish_100_CI_AS_SC">
                                          Turkish_100_CI_AS_SC
                                      </option>
                                      <option id="item72_94_option" value="Turkish_100_CI_AS_WS_SC">
                                          Turkish_100_CI_AS_WS_SC
                                      </option>
                                      <option id="item72_95_option" value="Turkish_100_CI_AS_KS_SC">
                                          Turkish_100_CI_AS_KS_SC
                                      </option>
                                      <option id="item72_96_option" value="Turkish_100_CI_AS_KS_WS_SC">
                                          Turkish_100_CI_AS_KS_WS_SC
                                      </option>
                                      <option id="item72_97_option" value="Turkish_100_CS_AI_SC">
                                          Turkish_100_CS_AI_SC
                                      </option>
                                      <option id="item72_98_option" value="Turkish_100_CS_AI_WS_SC">
                                          Turkish_100_CS_AI_WS_SC
                                      </option>
                                      <option id="item72_99_option" value="Turkish_100_CS_AI_KS_SC">
                                          Turkish_100_CS_AI_KS_SC
                                      </option>
                                      <option id="item72_100_option" value="Turkish_100_CS_AI_KS_WS_SC">
                                          Turkish_100_CS_AI_KS_WS_SC
                                      </option>
                                      <option id="item72_101_option" value="Turkish_100_CS_AS_SC">
                                          Turkish_100_CS_AS_SC
                                      </option>
                                      <option id="item72_102_option" value="Turkish_100_CS_AS_WS_SC">
                                          Turkish_100_CS_AS_WS_SC
                                      </option>
                                      <option id="item72_103_option" value="Turkish_100_CS_AS_KS_SC">
                                          Turkish_100_CS_AS_KS_SC
                                      </option>
                                      <option id="item72_104_option" value="Turkish_100_CS_AS_KS_WS_SC">
                                          Turkish_100_CS_AS_KS_WS_SC
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item177">
                              <div className="fb-grouplabel">
                                  <label id="item177_label_0"style={{display: 'inline'}}>Oracle Karakter Set</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="select177" id="item177_select_1" required="" data-hint="">
                                      <option id="item177_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item177_1_option" value="AL16UTF16">
                                          AL16UTF16
                                      </option>
                                      <option id="item177_2_option" value="AL24UTFFSS">
                                          AL24UTFFSS
                                      </option>
                                      <option id="item177_3_option" value="AL32UTF8">
                                          AL32UTF8
                                      </option>
                                      <option id="item177_4_option" value="AR8ADOS710">
                                          AR8ADOS710
                                      </option>
                                      <option id="item177_5_option" value="AR8ADOS710T">
                                          AR8ADOS710T
                                      </option>
                                      <option id="item177_6_option" value="AR8ADOS720">
                                          AR8ADOS720
                                      </option>
                                      <option id="item177_7_option" value="AR8ADOS720T">
                                          AR8ADOS720T
                                      </option>
                                      <option id="item177_8_option" value="AR8APTEC715">
                                          AR8APTEC715
                                      </option>
                                      <option id="item177_9_option" value="AR8APTEC715T">
                                          AR8APTEC715T
                                      </option>
                                      <option id="item177_10_option" value="AR8ARABICMAC">
                                          AR8ARABICMAC
                                      </option>
                                      <option id="item177_11_option" value="AR8ARABICMACS">
                                          AR8ARABICMACS
                                      </option>
                                      <option id="item177_12_option" value="AR8ARABICMACT">
                                          AR8ARABICMACT
                                      </option>
                                      <option id="item177_13_option" value="AR8ASMO8X">
                                          AR8ASMO8X
                                      </option>
                                      <option id="item177_14_option" value="AR8EBCDIC420S">
                                          AR8EBCDIC420S
                                      </option>
                                      <option id="item177_15_option" value="AR8EBCDICX">
                                          AR8EBCDICX
                                      </option>
                                      <option id="item177_16_option" value="AR8HPARABIC8T">
                                          AR8HPARABIC8T
                                      </option>
                                      <option id="item177_17_option" value="AR8ISO8859P6">
                                          AR8ISO8859P6
                                      </option>
                                      <option id="item177_18_option" value="AR8MSWIN1256">
                                          AR8MSWIN1256
                                      </option>
                                      <option id="item177_19_option" value="AR8MUSSAD768">
                                          AR8MUSSAD768
                                      </option>
                                      <option id="item177_20_option" value="AR8MUSSAD768T">
                                          AR8MUSSAD768T
                                      </option>
                                      <option id="item177_21_option" value="AR8NAFITHA711">
                                          AR8NAFITHA711
                                      </option>
                                      <option id="item177_22_option" value="AR8NAFITHA711T">
                                          AR8NAFITHA711T
                                      </option>
                                      <option id="item177_23_option" value="AR8NAFITHA721">
                                          AR8NAFITHA721
                                      </option>
                                      <option id="item177_24_option" value="AR8NAFITHA721T">
                                          AR8NAFITHA721T
                                      </option>
                                      <option id="item177_25_option" value="AR8SAKHR706">
                                          AR8SAKHR706
                                      </option>
                                      <option id="item177_26_option" value="AR8SAKHR707">
                                          AR8SAKHR707
                                      </option>
                                      <option id="item177_27_option" value="AR8SAKHR707T">
                                          AR8SAKHR707T
                                      </option>
                                      <option id="item177_28_option" value="AR8XBASIC">
                                          AR8XBASIC
                                      </option>
                                      <option id="item177_29_option" value="AZ8ISO8859P9E">
                                          AZ8ISO8859P9E
                                      </option>
                                      <option id="item177_30_option" value="BG8MSWIN">
                                          BG8MSWIN
                                      </option>
                                      <option id="item177_31_option" value="BG8PC437S">
                                          BG8PC437S
                                      </option>
                                      <option id="item177_32_option" value="BLT8CP921">
                                          BLT8CP921
                                      </option>
                                      <option id="item177_33_option" value="BLT8EBCDIC1112">
                                          BLT8EBCDIC1112
                                      </option>
                                      <option id="item177_34_option" value="BLT8EBCDIC1112S">
                                          BLT8EBCDIC1112S
                                      </option>
                                      <option id="item177_35_option" value="BLT8ISO8859P13">
                                          BLT8ISO8859P13
                                      </option>
                                      <option id="item177_36_option" value="BLT8MSWIN1257">
                                          BLT8MSWIN1257
                                      </option>
                                      <option id="item177_37_option" value="BLT8PC775">
                                          BLT8PC775
                                      </option>
                                      <option id="item177_38_option" value="BN8BSCII">
                                          BN8BSCII
                                      </option>
                                      <option id="item177_39_option" value="CDN8PC863">
                                          CDN8PC863
                                      </option>
                                      <option id="item177_40_option" value="CE8BS2000">
                                          CE8BS2000
                                      </option>
                                      <option id="item177_41_option" value="CEL8ISO8859P14">
                                          CEL8ISO8859P14
                                      </option>
                                      <option id="item177_42_option" value="CH7DEC">
                                          CH7DEC
                                      </option>
                                      <option id="item177_43_option" value="CL8BS2000">
                                          CL8BS2000
                                      </option>
                                      <option id="item177_44_option" value="CL8EBCDIC1025">
                                          CL8EBCDIC1025
                                      </option>
                                      <option id="item177_45_option" value="CL8EBCDIC1025C">
                                          CL8EBCDIC1025C
                                      </option>
                                      <option id="item177_46_option" value="CL8EBCDIC1025R">
                                          CL8EBCDIC1025R
                                      </option>
                                      <option id="item177_47_option" value="CL8EBCDIC1025S">
                                          CL8EBCDIC1025S
                                      </option>
                                      <option id="item177_48_option" value="CL8EBCDIC1025X">
                                          CL8EBCDIC1025X
                                      </option>
                                      <option id="item177_49_option" value="CL8EBCDIC1158">
                                          CL8EBCDIC1158
                                      </option>
                                      <option id="item177_50_option" value="CL8EBCDIC1158R">
                                          CL8EBCDIC1158R
                                      </option>
                                      <option id="item177_51_option" value="CL8ISO8859P5">
                                          CL8ISO8859P5
                                      </option>
                                      <option id="item177_52_option" value="CL8ISOIR111">
                                          CL8ISOIR111
                                      </option>
                                      <option id="item177_53_option" value="CL8KOI8R">
                                          CL8KOI8R
                                      </option>
                                      <option id="item177_54_option" value="CL8KOI8U">
                                          CL8KOI8U
                                      </option>
                                      <option id="item177_55_option" value="CL8MACCYRILLIC">
                                          CL8MACCYRILLIC
                                      </option>
                                      <option id="item177_56_option" value="CL8MACCYRILLICS">
                                          CL8MACCYRILLICS
                                      </option>
                                      <option id="item177_57_option" value="CL8MSWIN1251">
                                          CL8MSWIN1251
                                      </option>
                                      <option id="item177_58_option" value="D7DEC">
                                          D7DEC
                                      </option>
                                      <option id="item177_59_option" value="D7SIEMENS9780X">
                                          D7SIEMENS9780X
                                      </option>
                                      <option id="item177_60_option" value="D8BS2000">
                                          D8BS2000
                                      </option>
                                      <option id="item177_61_option" value="D8EBCDIC1141">
                                          D8EBCDIC1141
                                      </option>
                                      <option id="item177_62_option" value="D8EBCDIC273">
                                          D8EBCDIC273
                                      </option>
                                      <option id="item177_63_option" value="DK7SIEMENS9780X">
                                          DK7SIEMENS9780X
                                      </option>
                                      <option id="item177_64_option" value="DK8BS2000">
                                          DK8BS2000
                                      </option>
                                      <option id="item177_65_option" value="DK8EBCDIC1142">
                                          DK8EBCDIC1142
                                      </option>
                                      <option id="item177_66_option" value="DK8EBCDIC277">
                                          DK8EBCDIC277
                                      </option>
                                      <option id="item177_67_option" value="E7DEC">
                                          E7DEC
                                      </option>
                                      <option id="item177_68_option" value="E7SIEMENS9780X">
                                          E7SIEMENS9780X
                                      </option>
                                      <option id="item177_69_option" value="E8BS2000">
                                          E8BS2000
                                      </option>
                                      <option id="item177_70_option" value="EE8BS2000">
                                          EE8BS2000
                                      </option>
                                      <option id="item177_71_option" value="EE8EBCDIC870">
                                          EE8EBCDIC870
                                      </option>
                                      <option id="item177_72_option" value="EE8EBCDIC870C">
                                          EE8EBCDIC870C
                                      </option>
                                      <option id="item177_73_option" value="EE8EBCDIC870S">
                                          EE8EBCDIC870S
                                      </option>
                                      <option id="item177_74_option" value="EE8ISO8859P2">
                                          EE8ISO8859P2
                                      </option>
                                      <option id="item177_75_option" value="EE8MACCE">
                                          EE8MACCE
                                      </option>
                                      <option id="item177_76_option" value="EE8MACCES">
                                          EE8MACCES
                                      </option>
                                      <option id="item177_77_option" value="EE8MACCROATIAN">
                                          EE8MACCROATIAN
                                      </option>
                                      <option id="item177_78_option" value="EE8MACCROATIANS">
                                          EE8MACCROATIANS
                                      </option>
                                      <option id="item177_79_option" value="EE8MSWIN1250">
                                          EE8MSWIN1250
                                      </option>
                                      <option id="item177_80_option" value="EE8PC852">
                                          EE8PC852
                                      </option>
                                      <option id="item177_81_option" value="EEC8EUROASCI">
                                          EEC8EUROASCI
                                      </option>
                                      <option id="item177_82_option" value="EEC8EUROPA3">
                                          EEC8EUROPA3
                                      </option>
                                      <option id="item177_83_option" value="EL8DEC">
                                          EL8DEC
                                      </option>
                                      <option id="item177_84_option" value="EL8EBCDIC423R">
                                          EL8EBCDIC423R
                                      </option>
                                      <option id="item177_85_option" value="EL8EBCDIC875">
                                          EL8EBCDIC875
                                      </option>
                                      <option id="item177_86_option" value="EL8EBCDIC875R">
                                          EL8EBCDIC875R
                                      </option>
                                      <option id="item177_87_option" value="EL8EBCDIC875S">
                                          EL8EBCDIC875S
                                      </option>
                                      <option id="item177_88_option" value="EL8GCOS7">
                                          EL8GCOS7
                                      </option>
                                      <option id="item177_89_option" value="EL8ISO8859P7">
                                          EL8ISO8859P7
                                      </option>
                                      <option id="item177_90_option" value="EL8MACGREEK">
                                          EL8MACGREEK
                                      </option>
                                      <option id="item177_91_option" value="EL8MACGREEKS">
                                          EL8MACGREEKS
                                      </option>
                                      <option id="item177_92_option" value="EL8MSWIN1253">
                                          EL8MSWIN1253
                                      </option>
                                      <option id="item177_93_option" value="EL8PC437S">
                                          EL8PC437S
                                      </option>
                                      <option id="item177_94_option" value="EL8PC737">
                                          EL8PC737
                                      </option>
                                      <option id="item177_95_option" value="EL8PC851">
                                          EL8PC851
                                      </option>
                                      <option id="item177_96_option" value="EL8PC869">
                                          EL8PC869
                                      </option>
                                      <option id="item177_97_option" value="ET8MSWIN923">
                                          ET8MSWIN923
                                      </option>
                                      <option id="item177_98_option" value="F7DEC">
                                          F7DEC
                                      </option>
                                      <option id="item177_99_option" value="F7SIEMENS9780X">
                                          F7SIEMENS9780X
                                      </option>
                                      <option id="item177_100_option" value="F8BS2000">
                                          F8BS2000
                                      </option>
                                      <option id="item177_101_option" value="F8EBCDIC1147">
                                          F8EBCDIC1147
                                      </option>
                                      <option id="item177_102_option" value="F8EBCDIC297">
                                          F8EBCDIC297
                                      </option>
                                      <option id="item177_103_option" value="HU8ABMOD">
                                          HU8ABMOD
                                      </option>
                                      <option id="item177_104_option" value="HU8CWI2">
                                          HU8CWI2
                                      </option>
                                      <option id="item177_105_option" value="I7DEC">
                                          I7DEC
                                      </option>
                                      <option id="item177_106_option" value="I7SIEMENS9780X">
                                          I7SIEMENS9780X
                                      </option>
                                      <option id="item177_107_option" value="I8EBCDIC1144">
                                          I8EBCDIC1144
                                      </option>
                                      <option id="item177_108_option" value="I8EBCDIC280">
                                          I8EBCDIC280
                                      </option>
                                      <option id="item177_109_option" value="IN8ISCII">
                                          IN8ISCII
                                      </option>
                                      <option id="item177_110_option" value="IS8MACICELANDIC">
                                          IS8MACICELANDIC
                                      </option>
                                      <option id="item177_111_option" value="IS8MACICELANDICS">
                                          IS8MACICELANDICS
                                      </option>
                                      <option id="item177_112_option" value="IS8PC861">
                                          IS8PC861
                                      </option>
                                      <option id="item177_113_option" value="IW7IS960">
                                          IW7IS960
                                      </option>
                                      <option id="item177_114_option" value="IW8EBCDIC1086">
                                          IW8EBCDIC1086
                                      </option>
                                      <option id="item177_115_option" value="IW8EBCDIC424">
                                          IW8EBCDIC424
                                      </option>
                                      <option id="item177_116_option" value="IW8EBCDIC424S">
                                          IW8EBCDIC424S
                                      </option>
                                      <option id="item177_117_option" value="IW8ISO8859P8">
                                          IW8ISO8859P8
                                      </option>
                                      <option id="item177_118_option" value="IW8MACHEBREW">
                                          IW8MACHEBREW
                                      </option>
                                      <option id="item177_119_option" value="IW8MACHEBREWS">
                                          IW8MACHEBREWS
                                      </option>
                                      <option id="item177_120_option" value="IW8MSWIN1255">
                                          IW8MSWIN1255
                                      </option>
                                      <option id="item177_121_option" value="IW8PC1507">
                                          IW8PC1507
                                      </option>
                                      <option id="item177_122_option" value="JA16DBCS">
                                          JA16DBCS
                                      </option>
                                      <option id="item177_123_option" value="JA16DBCSFIXED">
                                          JA16DBCSFIXED
                                      </option>
                                      <option id="item177_124_option" value="JA16EBCDIC930">
                                          JA16EBCDIC930
                                      </option>
                                      <option id="item177_125_option" value="JA16EUC">
                                          JA16EUC
                                      </option>
                                      <option id="item177_126_option" value="JA16EUCFIXED">
                                          JA16EUCFIXED
                                      </option>
                                      <option id="item177_127_option" value="JA16EUCTILDE">
                                          JA16EUCTILDE
                                      </option>
                                      <option id="item177_128_option" value="JA16EUCYEN">
                                          JA16EUCYEN
                                      </option>
                                      <option id="item177_129_option" value="JA16MACSJIS">
                                          JA16MACSJIS
                                      </option>
                                      <option id="item177_130_option" value="JA16SJIS">
                                          JA16SJIS
                                      </option>
                                      <option id="item177_131_option" value="JA16SJISFIXED">
                                          JA16SJISFIXED
                                      </option>
                                      <option id="item177_132_option" value="JA16SJISTILDE">
                                          JA16SJISTILDE
                                      </option>
                                      <option id="item177_133_option" value="JA16SJISYEN">
                                          JA16SJISYEN
                                      </option>
                                      <option id="item177_134_option" value="JA16VMS">
                                          JA16VMS
                                      </option>
                                      <option id="item177_135_option" value="KO16DBCS">
                                          KO16DBCS
                                      </option>
                                      <option id="item177_136_option" value="KO16DBCSFIXED">
                                          KO16DBCSFIXED
                                      </option>
                                      <option id="item177_137_option" value="KO16KSC5601">
                                          KO16KSC5601
                                      </option>
                                      <option id="item177_138_option" value="KO16KSC5601FIXED">
                                          KO16KSC5601FIXED
                                      </option>
                                      <option id="item177_139_option" value="KO16KSCCS">
                                          KO16KSCCS
                                      </option>
                                      <option id="item177_140_option" value="KO16MSWIN949">
                                          KO16MSWIN949
                                      </option>
                                      <option id="item177_141_option" value="LA8ISO6937">
                                          LA8ISO6937
                                      </option>
                                      <option id="item177_142_option" value="LA8PASSPORT">
                                          LA8PASSPORT
                                      </option>
                                      <option id="item177_143_option" value="LT8MSWIN921">
                                          LT8MSWIN921
                                      </option>
                                      <option id="item177_144_option" value="LT8PC772">
                                          LT8PC772
                                      </option>
                                      <option id="item177_145_option" value="LT8PC774">
                                          LT8PC774
                                      </option>
                                      <option id="item177_146_option" value="LV8PC1117">
                                          LV8PC1117
                                      </option>
                                      <option id="item177_147_option" value="LV8PC8LR">
                                          LV8PC8LR
                                      </option>
                                      <option id="item177_148_option" value="LV8RST104090">
                                          LV8RST104090
                                      </option>
                                      <option id="item177_149_option" value="N7SIEMENS9780X">
                                          N7SIEMENS9780X
                                      </option>
                                      <option id="item177_150_option" value="N8PC865">
                                          N8PC865
                                      </option>
                                      <option id="item177_151_option" value="NDK7DEC">
                                          NDK7DEC
                                      </option>
                                      <option id="item177_152_option" value="NE8ISO8859P10">
                                          NE8ISO8859P10
                                      </option>
                                      <option id="item177_153_option" value="NEE8ISO8859P4">
                                          NEE8ISO8859P4
                                      </option>
                                      <option id="item177_154_option" value="NL7DEC">
                                          NL7DEC
                                      </option>
                                      <option id="item177_155_option" value="RU8BESTA">
                                          RU8BESTA
                                      </option>
                                      <option id="item177_156_option" value="RU8PC855">
                                          RU8PC855
                                      </option>
                                      <option id="item177_157_option" value="RU8PC866">
                                          RU8PC866
                                      </option>
                                      <option id="item177_158_option" value="S7DEC">
                                          S7DEC
                                      </option>
                                      <option id="item177_159_option" value="S7SIEMENS9780X">
                                          S7SIEMENS9780X
                                      </option>
                                      <option id="item177_160_option" value="S8BS2000">
                                          S8BS2000
                                      </option>
                                      <option id="item177_161_option" value="S8EBCDIC1143">
                                          S8EBCDIC1143
                                      </option>
                                      <option id="item177_162_option" value="S8EBCDIC278">
                                          S8EBCDIC278
                                      </option>
                                      <option id="item177_163_option" value="SE8ISO8859P3">
                                          SE8ISO8859P3
                                      </option>
                                      <option id="item177_164_option" value="SF7ASCII">
                                          SF7ASCII
                                      </option>
                                      <option id="item177_165_option" value="SF7DEC">
                                          SF7DEC
                                      </option>
                                      <option id="item177_166_option" value="TH8MACTHAI">
                                          TH8MACTHAI
                                      </option>
                                      <option id="item177_167_option" value="TH8MACTHAIS">
                                          TH8MACTHAIS
                                      </option>
                                      <option id="item177_168_option" value="TH8TISASCII">
                                          TH8TISASCII
                                      </option>
                                      <option id="item177_169_option" value="TH8TISEBCDIC">
                                          TH8TISEBCDIC
                                      </option>
                                      <option id="item177_170_option" value="TH8TISEBCDICS">
                                          TH8TISEBCDICS
                                      </option>
                                      <option id="item177_171_option" value="TR7DEC">
                                          TR7DEC
                                      </option>
                                      <option id="item177_172_option" value="TR8DEC">
                                          TR8DEC
                                      </option>
                                      <option id="item177_173_option" value="TR8EBCDIC1026">
                                          TR8EBCDIC1026
                                      </option>
                                      <option id="item177_174_option" value="TR8EBCDIC1026S">
                                          TR8EBCDIC1026S
                                      </option>
                                      <option id="item177_175_option" value="TR8MACTURKISH">
                                          TR8MACTURKISH
                                      </option>
                                      <option id="item177_176_option" value="TR8MACTURKISHS">
                                          TR8MACTURKISHS
                                      </option>
                                      <option id="item177_177_option" value="TR8MSWIN1254">
                                          TR8MSWIN1254
                                      </option>
                                      <option id="item177_178_option" value="TR8PC857">
                                          TR8PC857
                                      </option>
                                      <option id="item177_179_option" value="US7ASCII">
                                          US7ASCII
                                      </option>
                                      <option id="item177_180_option" value="US8BS2000">
                                          US8BS2000
                                      </option>
                                      <option id="item177_181_option" value="US8ICL">
                                          US8ICL
                                      </option>
                                      <option id="item177_182_option" value="US8PC437">
                                          US8PC437
                                      </option>
                                      <option id="item177_183_option" value="UTF8">
                                          UTF8
                                      </option>
                                      <option id="item177_184_option" value="UTFE">
                                          UTFE
                                      </option>
                                      <option id="item177_185_option" value="VN8MSWIN1258">
                                          VN8MSWIN1258
                                      </option>
                                      <option id="item177_186_option" value="VN8VN3">
                                          VN8VN3
                                      </option>
                                      <option id="item177_187_option" value="WE8BS2000">
                                          WE8BS2000
                                      </option>
                                      <option id="item177_188_option" value="WE8BS2000E">
                                          WE8BS2000E
                                      </option>
                                      <option id="item177_189_option" value="WE8BS2000L5">
                                          WE8BS2000L5
                                      </option>
                                      <option id="item177_190_option" value="WE8DEC">
                                          WE8DEC
                                      </option>
                                      <option id="item177_191_option" value="WE8DG">
                                          WE8DG
                                      </option>
                                      <option id="item177_192_option" value="WE8EBCDIC1047">
                                          WE8EBCDIC1047
                                      </option>
                                      <option id="item177_193_option" value="WE8EBCDIC1047E">
                                          WE8EBCDIC1047E
                                      </option>
                                      <option id="item177_194_option" value="WE8EBCDIC1140">
                                          WE8EBCDIC1140
                                      </option>
                                      <option id="item177_195_option" value="WE8EBCDIC1140C">
                                          WE8EBCDIC1140C
                                      </option>
                                      <option id="item177_196_option" value="WE8EBCDIC1145">
                                          WE8EBCDIC1145
                                      </option>
                                      <option id="item177_197_option" value="WE8EBCDIC1146">
                                          WE8EBCDIC1146
                                      </option>
                                      <option id="item177_198_option" value="WE8EBCDIC1148">
                                          WE8EBCDIC1148
                                      </option>
                                      <option id="item177_199_option" value="WE8EBCDIC1148C">
                                          WE8EBCDIC1148C
                                      </option>
                                      <option id="item177_200_option" value="WE8EBCDIC284">
                                          WE8EBCDIC284
                                      </option>
                                      <option id="item177_201_option" value="WE8EBCDIC285">
                                          WE8EBCDIC285
                                      </option>
                                      <option id="item177_202_option" value="WE8EBCDIC37">
                                          WE8EBCDIC37
                                      </option>
                                      <option id="item177_203_option" value="WE8EBCDIC37C">
                                          WE8EBCDIC37C
                                      </option>
                                      <option id="item177_204_option" value="WE8EBCDIC500">
                                          WE8EBCDIC500
                                      </option>
                                      <option id="item177_205_option" value="WE8EBCDIC500C">
                                          WE8EBCDIC500C
                                      </option>
                                      <option id="item177_206_option" value="WE8EBCDIC871">
                                          WE8EBCDIC871
                                      </option>
                                      <option id="item177_207_option" value="WE8EBCDIC924">
                                          WE8EBCDIC924
                                      </option>
                                      <option id="item177_208_option" value="WE8GCOS7">
                                          WE8GCOS7
                                      </option>
                                      <option id="item177_209_option" value="WE8HP">
                                          WE8HP
                                      </option>
                                      <option id="item177_210_option" value="WE8ICL">
                                          WE8ICL
                                      </option>
                                      <option id="item177_211_option" value="WE8ISO8859P1">
                                          WE8ISO8859P1
                                      </option>
                                      <option id="item177_212_option" value="WE8ISO8859P15">
                                          WE8ISO8859P15
                                      </option>
                                      <option id="item177_213_option" value="WE8ISO8859P9">
                                          WE8ISO8859P9
                                      </option>
                                      <option id="item177_214_option" value="WE8ISOICLUK">
                                          WE8ISOICLUK
                                      </option>
                                      <option id="item177_215_option" value="WE8MACROMAN8">
                                          WE8MACROMAN8
                                      </option>
                                      <option id="item177_216_option" value="WE8MACROMAN8S">
                                          WE8MACROMAN8S
                                      </option>
                                      <option id="item177_217_option" value="WE8MSWIN1252">
                                          WE8MSWIN1252
                                      </option>
                                      <option id="item177_218_option" value="WE8NCR4970">
                                          WE8NCR4970
                                      </option>
                                      <option id="item177_219_option" value="WE8NEXTSTEP">
                                          WE8NEXTSTEP
                                      </option>
                                      <option id="item177_220_option" value="WE8PC850">
                                          WE8PC850
                                      </option>
                                      <option id="item177_221_option" value="WE8PC858">
                                          WE8PC858
                                      </option>
                                      <option id="item177_222_option" value="WE8PC860">
                                          WE8PC860
                                      </option>
                                      <option id="item177_223_option" value="WE8ROMAN8">
                                          WE8ROMAN8
                                      </option>
                                      <option id="item177_224_option" value="YUG7ASCII">
                                          YUG7ASCII
                                      </option>
                                      <option id="item177_225_option" value="ZHS16CGB231280">
                                          ZHS16CGB231280
                                      </option>
                                      <option id="item177_226_option" value="ZHS16CGB231280FIXED">
                                          ZHS16CGB231280FIXED
                                      </option>
                                      <option id="item177_227_option" value="ZHS16DBCS">
                                          ZHS16DBCS
                                      </option>
                                      <option id="item177_228_option" value="ZHS16DBCSFIXED">
                                          ZHS16DBCSFIXED
                                      </option>
                                      <option id="item177_229_option" value="ZHS16GBK">
                                          ZHS16GBK
                                      </option>
                                      <option id="item177_230_option" value="ZHS16GBKFIXED">
                                          ZHS16GBKFIXED
                                      </option>
                                      <option id="item177_231_option" value="ZHS16MACCGB231280">
                                          ZHS16MACCGB231280
                                      </option>
                                      <option id="item177_232_option" value="ZHS32GB18030">
                                          ZHS32GB18030
                                      </option>
                                      <option id="item177_233_option" value="ZHT16BIG5">
                                          ZHT16BIG5
                                      </option>
                                      <option id="item177_234_option" value="ZHT16BIG5FIXED">
                                          ZHT16BIG5FIXED
                                      </option>
                                      <option id="item177_235_option" value="ZHT16CCDC">
                                          ZHT16CCDC
                                      </option>
                                      <option id="item177_236_option" value="ZHT16DBCS">
                                          ZHT16DBCS
                                      </option>
                                      <option id="item177_237_option" value="ZHT16DBCSFIXED">
                                          ZHT16DBCSFIXED
                                      </option>
                                      <option id="item177_238_option" value="ZHT16DBT">
                                          ZHT16DBT
                                      </option>
                                      <option id="item177_239_option" value="ZHT16HKSCS">
                                          ZHT16HKSCS
                                      </option>
                                      <option id="item177_240_option" value="ZHT16HKSCS31">
                                          ZHT16HKSCS31
                                      </option>
                                      <option id="item177_241_option" value="ZHT16MSWIN950">
                                          ZHT16MSWIN950
                                      </option>
                                      <option id="item177_242_option" value="ZHT32EUC">
                                          ZHT32EUC
                                      </option>
                                      <option id="item177_243_option" value="ZHT32EUCFIXED">
                                          ZHT32EUCFIXED
                                      </option>
                                      <option id="item177_244_option" value="ZHT32SOPS">
                                          ZHT32SOPS
                                      </option>
                                      <option id="item177_245_option" value="ZHT32TRIS">
                                          ZHT32TRIS
                                      </option>
                                      <option id="item177_246_option" value="ZHT32TRISFIXED">
                                          ZHT32TRISFIXED
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item77" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item77_label_0"style={{display: 'inline'}}>Sql Instance Adı</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="VeritabanAd" id="item77_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item160" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item160_label_0"style={{display: 'inline'}}>Oracle Instance Adı</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text160" id="item160_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item73">
                              <div className="fb-grouplabel">
                                  <label id="item73_label_0"style={{display: 'inline'}}>Yüksek Erişebilirlik Çözümleri</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="YksekEriebilirlikzmleri" id="item73_select_1" required="" data-hint="">
                                      <option id="item73_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item73_1_option" value="Hizmet Alınmayacak">
                                          Hizmet Alınmayacak
                                      </option>
                                      <option id="item73_2_option" value="Oracle Log Shipping">
                                          Oracle Log Shipping
                                      </option>
                                      <option id="item73_3_option" value="Oracle Dataguard (pasif)">
                                          Oracle Dataguard (pasif)
                                      </option>
                                      <option id="item73_4_option" value="Oracle Dataguard (aktif >= 11g)">
                                          Oracle Dataguard (aktif &gt;= 11g)
                                      </option>
                                      <option id="item73_5_option" value="Oracle Streams">
                                          Oracle Streams
                                      </option>
                                      <option id="item73_6_option" value="Oracle Golden Gate">
                                          Oracle Golden Gate
                                      </option>
                                      <option id="item73_7_option" value="SQL Server Mirroring">
                                          SQL Server Mirroring
                                      </option>
                                      <option id="item73_8_option" value="SQL Server Log Shipping">
                                          SQL Server Log Shipping
                                      </option>
                                      <option id="item73_9_option" value="SQL Server 2012 Always on">
                                          SQL Server 2012 Always on
                                      </option>
                                      <option id="item73_10_option" value="SQL Server Replication">
                                          SQL Server Replication
                                      </option>
                                      <option id="item73_11_option" value="MySQL Replication">
                                          MySQL Replication
                                      </option>
                                      <option id="item73_12_option" value="IBM DB2 HADR">
                                          IBM DB2 HADR
                                      </option>
                                      <option id="item73_13_option" value="Storage Replication">
                                          Storage Replication
                                      </option>
                                      <option id="item73_14_option" value="Sybase Replication">
                                          Sybase Replication
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item74" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item74_label_0"style={{display: 'inline'}}>Veritabanı Connectivity</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="VeritabanConnectivity" id="item74_select_1" required="" data-hint="">
                                      <option id="item74_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item74_1_option" value="32 bit">
                                          32 bit
                                      </option>
                                      <option id="item74_2_option" value="64 bit">
                                          64 bit
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-three-column fb-100-item-column fb_cond_applied" id="item152" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item152_label_0"style={{display: 'inline'}}>Bileşenler</label>
                              </div>
                              <div className="fb-checkbox">
                                  <label id="item152_0_label">
                                      <input name="bilesenler[]" id="item152_0_checkbox" type="checkbox" value="SQL Server Analysis Services" data-hint=""/>
                                      <span className="fb-fieldlabel" id="item152_0_span">
                                                    SQL Server Analysis
                                                    Services
                                                </span>
                                  </label>
                                  <label id="item152_1_label">
                                      <input name="bilesenler[]" id="item152_1_checkbox" type="checkbox" value="SQL Server Integration Services"/>
                                      <span className="fb-fieldlabel" id="item152_1_span">
                                                    SQL
                                                    Server Integration Services
                                                </span>
                                  </label>
                                  <label id="item152_2_label">
                                      <input name="bilesenler[]" id="item152_2_checkbox" type="checkbox" value="SQL Server Reporting Services"/>
                                      <span className="fb-fieldlabel" id="item152_2_span">
                                                    SQL
                                                    Server Reporting Services
                                                </span>
                                  </label>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item84" style={{height: '50px', min_height: '5px'}}>
                              <div className="fb-spacer">
                                  <div id="item84_div_0"></div>
                              </div>
                          </div>
                          <div className="fb-item fb_cond_applied" id="item85">
                              <div className="fb-sectionbreak">
                                  <hr style={{max_width: '960px'}}/>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item86">
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>            VERİ DEPOLAMA YÖNETİMİ           </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item87">
                              <div className="fb-grouplabel">
                                  <label id="item87_label_0"style={{display: 'inline'}}>
                                      Filesystem veya Raw device dağılımı (C = 100g, D=200g gibi) *
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="FilesystemveyaRawdevicedalm" id="item87_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item88">
                              <div className="fb-grouplabel">
                                  <label id="item88_label_0"style={{display: 'inline'}}>HBA Bilgileri ( HBA Marka/Model Bilgisi) *</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="HBABilgileriHBAMarkaModelBilgisi" id="item88_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item89" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item89_label_0"style={{display: 'inline'}}>
                                      HBA Redundancy Onayı ( Tek HBA/port olması durumunda yedekliliğin sağlanamaması nedeniyle müşteriden onay alınması gerekir)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="HBARedundancyOnayTekHBAportolmasdurumundayedekliliinsalanamamasnedeniylemteridenonayalnmasgerekmekt" id="item89_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item90">
                              <div className="fb-grouplabel">
                                  <label id="item90_label_0"style={{display: 'inline'}}>DR hizmeti isteniyor mu?</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="DRhizmetiisteniyormu" id="item90_select_1" required="" data-hint="">
                                      <option id="item90_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item90_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item90_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item91">
                              <div className="fb-grouplabel">
                                  <label id="item91_label_0"style={{display: 'inline'}}>
                                      DR replikasyon RPO değeri (RPO ve saatlik veri değişimi bilgileri müşteri tarafından verilmeli)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="DRreplikasyonRPOdeeriRPOvesaatlikverideiimibilgilerimteritarafndanverilmeli" id="item91_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item93" style={{height: '50px', min_height: '5px'}}>
                              <div className="fb-spacer">
                                  <div id="item93_div_0"></div>
                              </div>
                          </div>
                          <div className="fb-item fb_cond_applied" id="item94">
                              <div className="fb-sectionbreak">
                                  <hr style={{max_width: '960px'}}/>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item95">
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>            SAP YÖNETİMİ           </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item96">
                              <div className="fb-grouplabel">
                                  <label id="item96_label_0"style={{display: 'inline'}}>
                                      SAP Sistem versiyonları (ERP 6.0 EHP 5,BW 7.31,BO 4.0 vs.)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SAPSistemversiyonlarERP60EHP5BW731BO40vs" id="item96_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item97" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item97_label_0"style={{display: 'inline'}}>
                                      SAP Platform Bilgisi ( OS, DB,Cluster, HA, Application Server)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SAPPlatformBilgisiOSDBClusterHAApplicationServer" id="item97_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item98">
                              <div className="fb-grouplabel">
                                  <label id="item98_label_0"style={{display: 'inline'}}>Conc.Kullanıcı sayısı</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="ConcKullancsays" id="item98_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item99" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item99_label_0"style={{display: 'inline'}}>SAP sistem mimarisi (İkili,Üçlü yapı)</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SAPsistemmimarisikililyap" id="item99_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item100">
                              <div className="fb-grouplabel">
                                  <label id="item100_label_0"style={{display: 'inline'}}>Entegre olan sistemlerin bilgisi (nonsap, sap)</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="Entegreolansistemlerinbilgisinonsapsap" id="item100_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item101">
                              <div className="fb-grouplabel">
                                  <label id="item101_label_0"style={{display: 'inline'}}>Veritabanı büyüklüğü ve büyüme trendi</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="Veritabanbyklvebymetrendi" id="item101_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off"/>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item102" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item102_label_0"style={{display: 'inline'}}>
                                      Yakın zamanda devreye alınmış ve/veya alınacak modül/kullanıcısı bilgisi
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="Yaknzamandadevreyealnmveveyaalnacakmodlkullancsbilgisi" id="item102_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item103" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item103_label_0"style={{display: 'inline'}}>
                                      En son ne zaman EHP veya Versiyon upgrade yapıldı?
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="EnsonnezamanEHPveyaVersiyonupgradeyapld" id="item103_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item104" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item104_label_0"style={{display: 'inline'}}>
                                      Sistem üzerinde birden fazla client var mı? (Evet ise lütfen ek bilgi veriniz)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="SistemzerindebirdenfazlaclientvarmEvetiseltfenekbilgiveriniz" id="item104_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item105" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item105_label_0"style={{display: 'inline'}}>Kullanııcların performans hakkındaki yorumu ?</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="Kullanclarnperformanshakkndakiyorumu" id="item105_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item106" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item106_label_0"style={{display: 'inline'}}>
                                      Planlı kesinti takvimi ve SAP'ın business kritikliği hakkında bilgi
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text106" id="item106_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item107">
                              <div className="fb-grouplabel">
                                  <label id="item107_label_0"style={{display: 'inline'}}>
                                      Arşivleme yapılıyor mu? (Evet ise lütfen ek bilgi veriniz)
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="ArivlemeyaplyormuEvetiseltfenekbilgiveriniz" id="item107_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item109" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item109_label_0"style={{display: 'inline'}}>Solman sistemi var mı?</label>
                              </div>
                              <div className="fb-textarea">
                                  <textarea name="Solmansistemivarm" id="item109_textarea_1" style={{resize: 'none'}} maxLength="10000" placeholder="" data-hint=""></textarea>
                              </div>
                          </div>
                          <div className="fb-item fb_cond_applied" id="item110">
                              <div className="fb-sectionbreak">
                                  <hr style={{max_width: '960px'}}/>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item111">
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>            Yedekleme Yönetimi           </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item126" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item126_label_0"style={{display: 'inline'}}>Sistem Diski Yedekleme Periyodu</label>
                              </div>
                              <div className="fb-listbox">
                                  <select name="DefaulthariciistenilenBackupSaklamaSresi[]" id="item126_select_1" required="" multiple="" data-hint="Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün,  60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2">
                                      <option id="item126_0_option" value="Haftalık Incremental + Aylık Full (Ayda 4yedek)" selected="">
                                          Haftalık Incremental + Aylık Full (Ayda 4yedek)
                                      </option>
                                      <option id="item126_1_option" value="Günlük (Hergün Full - Aylık 30 Yedek)">
                                          Günlük (Hergün Full - Aylık 30 Yedek)
                                      </option>
                                      <option id="item126_2_option" value="Aylık Full ( Ayda 1 Yedek )">
                                          Aylık Full ( Ayda 1 Yedek )
                                      </option>
                                      <option id="item126_3_option" value="Günlük incremental + haftalık full ( Ayda 30 Yedek )">
                                          Günlük incremental + haftalık full ( Ayda 30 Yedek )
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(237, 12, 12); font-style: normal; font-weight: normal;"
                              >
                                  Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün, 60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item116" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item116_label_0"style={{display: 'inline'}}>
                                      Yedeklenmesi istenen başka Disk / Klasör / Dosya var mı ?
                                  </label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="YedeklenmesiistenenbakaDiskKlasrDosyavarm" id="item116_select_1" required="" data-hint="Bu alanda sadece sistem diski ( C:/ ) dışında spesifik olarak yedeklenmesi istenen diskler belirtilmelidir.  ">
                                      <option id="item116_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item116_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item116_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(136, 136, 136); font-style: normal; font-weight: normal;"
                              >
                                  Bu alanda sadece sistem diski ( C:/ ) dışında spesifik olarak yedeklenmesi istenen diskler belirtilmelidir.
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb-three-column fb_cond_applied" id="item117" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item117_label_0"style={{display: 'inline'}}>MS işletim sistemi için</label>
                              </div>
                              <div className="fb-checkbox">
                                  <label id="item117_0_label">
                                      <input name="MSiletimsistemiiin[]" id="item117_0_checkbox" type="checkbox" value="Sanal Sunucu (VMDK backup) full image" data-hint=""/>
                                      <span className="fb-fieldlabel" id="item117_0_span">
                                                    Sanal Sunucu (VMDK
                                                    backup) full image
                                                </span>
                                  </label>
                                  <label id="item117_1_label">
                                      <input name="MSiletimsistemiiin[]" id="item117_1_checkbox" type="checkbox" value="D:\"/>
                                      <span className="fb-fieldlabel" id="item117_1_span">D:\</span>
                                  </label>
                                  <label id="item117_2_label">
                                      <input name="MSiletimsistemiiin[]" id="item117_2_checkbox" type="checkbox" value="E.\"/>
                                      <span className="fb-fieldlabel" id="item117_2_span">E.\</span>
                                  </label>
                                  <label id="item117_3_label">
                                      <input name="MSiletimsistemiiin[]" id="item117_3_checkbox" type="checkbox" value="F:\"/>
                                      <span className="fb-fieldlabel" id="item117_3_span">F:\</span>
                                  </label>
                                  <label id="item117_4_label">
                                      <input name="MSiletimsistemiiin[]" id="item117_4_checkbox" type="checkbox" value="…. Diğer (Sürücü belirtilmeli)"/>
                                      <span className="fb-fieldlabel" id="item117_4_span">…. Diğer (Sürücü belirtilmeli)</span>
                                  </label>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item119" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item119_label_0"style={{display: 'inline'}}>Yedeklenmesi istenen Diğer Alanlar</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="YedeklenmesiistenenDierAlanlar" id="item119_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item158" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item158_label_0"style={{display: 'inline'}}>Opsiyonel Disklerin Yedekleme Periyodu</label>
                              </div>
                              <div className="fb-listbox">
                                  <select name="select158[]" id="item158_select_1" required="" multiple="" data-hint="Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün,  60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2">
                                      <option id="item158_0_option" value="Günlük incremental + haftalık full ( Ayda 30 Yedek )" selected="">
                                          Günlük incremental + haftalık full ( Ayda 30 Yedek )
                                      </option>
                                      <option id="item158_1_option" value="Günlük (Hergün Full - Aylık 30 Yedek)">
                                          Günlük (Hergün Full - Aylık 30 Yedek)
                                      </option>
                                      <option id="item158_2_option" value="Haftalık Incremental + Aylık Full (Ayda 4yedek)">
                                          Haftalık Incremental + Aylık Full (Ayda 4yedek)
                                      </option>
                                      <option id="item158_3_option" value="Aylık Full ( Ayda 1 Yedek )">
                                          Aylık Full ( Ayda 1 Yedek )
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(237, 12, 12); font-style: normal; font-weight: normal;"
                              >
                                  Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün, 60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb-three-column fb_cond_applied" id="item118" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item118_label_0"style={{display: 'inline'}}>Unix\lnxİşletim Sistemleri İçin</label>
                              </div>
                              <div className="fb-checkbox">
                                  <label id="item118_0_label">
                                      <input name="checkbox118[]" id="item118_0_checkbox" type="checkbox" value="/Mount Point (nfs alanı)" data-hint="Root diski unx\lnx  İşletim Sistemleri İçin standart olarak backuplanan alandır."/>
                                      <span className="fb-fieldlabel" id="item118_0_span">
                                                    /Mount Point (nfs
                                                    alanı)
                                                </span>
                                  </label>
                                  <label id="item118_1_label">
                                      <input name="checkbox118[]" id="item118_1_checkbox" type="checkbox" value="/mnt"/>
                                      <span className="fb-fieldlabel" id="item118_1_span">/mnt</span>
                                  </label>
                                  <label id="item118_2_label">
                                      <input name="checkbox118[]" id="item118_2_checkbox" type="checkbox" value="opt"/>
                                      <span className="fb-fieldlabel" id="item118_2_span">opt</span>
                                  </label>
                                  <label id="item118_3_label">
                                      <input name="checkbox118[]" id="item118_3_checkbox" type="checkbox" value="/var"/>
                                      <span className="fb-fieldlabel" id="item118_3_span">/var</span>
                                  </label>
                                  <label id="item118_4_label">
                                      <input name="checkbox118[]" id="item118_4_checkbox" type="checkbox" value="…. Diğer (Sürücü belirtilmeli)"/>
                                      <span className="fb-fieldlabel" id="item118_4_span">
                                                    ….
                                                    Diğer (Sürücü belirtilmeli)
                                                </span>
                                  </label>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(136, 136, 136); font-style: normal; font-weight: normal;"
                              >
                                  Root diski unx\lnx İşletim Sistemleri İçin standart olarak backuplanan alandır.
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item122">
                              <div className="fb-grouplabel">
                                  <label id="item122_label_0"style={{display: 'inline'}}>Veritabanı</label>
                              </div>
                              <div className="fb-listbox">
                                  <select name="Veritaban[]" id="item122_select_1" required="" multiple="" data-hint="">
                                      <option id="item122_0_option" value="Microsoft SQL Server">
                                          Microsoft SQL Server
                                      </option>
                                      <option id="item122_1_option" value="MySQL">
                                          MySQL
                                      </option>
                                      <option id="item122_2_option" value="IBM DB2" selected="">
                                          IBM DB2
                                      </option>
                                      <option id="item122_3_option" value="Oracle">
                                          Oracle
                                      </option>
                                      <option id="item122_4_option" value="SAP Hana">
                                          SAP Hana
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item157" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item157_label_0"style={{display: 'inline'}}>Sql Veritabanı Yedekleme Periyodu</label>
                              </div>
                              <div className="fb-listbox">
                                  <select name="select157[]" id="item157_select_1" required="" multiple="" data-hint="Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün,  60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2">
                                      <option id="item157_0_option" value="Günlük (Hergün Full - Aylık 30 Yedek)" selected="">
                                          Günlük (Hergün Full - Aylık 30 Yedek)
                                      </option>
                                      <option id="item157_1_option" value="Haftalık Incremental + Aylık Full (Ayda 4yedek)">
                                          Haftalık Incremental + Aylık Full (Ayda 4yedek)
                                      </option>
                                      <option id="item157_2_option" value="Aylık Full ( Ayda 1 Yedek )">
                                          Aylık Full ( Ayda 1 Yedek )
                                      </option>
                                      <option id="item157_3_option" value="Günlük incremental + haftalık full ( Ayda 30 Yedek )">
                                          Günlük incremental + haftalık full ( Ayda 30 Yedek )
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                              //style="color: rgb(237, 12, 12); font-style: normal; font-weight: normal;"
                              >
                                  Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün, 60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item161" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item161_label_0"style={{display: 'inline'}}>Oracle Veritanı Yedekleme Periyodu</label>
                              </div>
                              <div className="fb-listbox">
                                  <select name="select161[]" id="item161_select_1" required="" multiple="" data-hint="Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün,  60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2">
                                      <option id="item161_0_option" value="Günlük (Hergün Full - Aylık 30 Yedek)" selected="">
                                          Günlük (Hergün Full - Aylık 30 Yedek)
                                      </option>
                                      <option id="item161_1_option" value="Haftalık Incremental + Aylık Full (Ayda 4yedek)">
                                          Haftalık Incremental + Aylık Full (Ayda 4yedek)
                                      </option>
                                      <option id="item161_2_option" value="Aylık Full ( Ayda 1 Yedek )">
                                          Aylık Full ( Ayda 1 Yedek )
                                      </option>
                                      <option id="item161_3_option" value="Günlük incremental + haftalık full ( Ayda 30 Yedek )">
                                          Günlük incremental + haftalık full ( Ayda 30 Yedek )
                                      </option>
                                  </select>
                              </div>
                              <div className="fb-hint" 
                            //   style="color: rgb(237, 12, 12); font-style: normal; font-weight: normal;"
                              >
                                  Default saklama süresi günlük ve haftalık yedeklerin 30 gün, 30 gün sonra yedek (expire olur ve geri dönülemez. Aylık alınan yedeklerin saklanma süresi 60 gün, 60 içinde alınan ilk yedek expire olduktan sonra sistemde kalan yedek sayısı 2
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item123">
                              <div className="fb-grouplabel">
                                  <label id="item123_label_0"style={{display: 'inline'}}>Yedeklenecek disk alanı toplam size (GB)</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="YedeklenecekdiskalantoplamsizeGB" id="item123_number_1" required="" type="number" min="1" max="999999999" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item125" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item125_label_0"style={{display: 'inline'}}>Öngörülebilen Yıllık Büyüme Oranı (%)</label>
                              </div>
                              <div className="fb-input-number">
                                  <input name="number125" id="item125_number_1" type="number" min="1" max="100" step="1" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item127" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item127_label_0"style={{display: 'inline'}}>Uzun Süreli Yedek Saklama (Vault) Talebi</label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="UzunSreliYedekSaklamaVaultTalebi" id="item127_select_1" required="" data-hint="">
                                      <option id="item127_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item127_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item127_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item130">
                              <div className="fb-grouplabel">
                                  <label id="item130_label_0"style={{display: 'inline'}}>File System Vault</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text130" id="item130_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="Örnek 2 ay, 3 ay, 6 ay, 1 yıl, sonsuz" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(240, 14, 14); font-style: normal; font-weight: bold;"
                                >
                                      Örnek 2 ay, 3 ay, 6 ay, 1 yıl, sonsuz
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item131" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item131_label_0"style={{display: 'inline'}}>Vault Ek İstek</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text131" id="item131_text_1" type="text" maxLength="254" placeholder="" data-hint="C:\inetpub sadece 1 yıl saklansın" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                 // style="color: rgb(245, 10, 10); font-style: normal; font-weight: bold;"
                                  >
                                      C:\inetpub sadece 1 yıl saklansın
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item132" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item132_label_0"style={{display: 'inline'}}>File System Vault Peryodu</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text132" id="item132_text_1" type="text" maxLength="254" placeholder="" data-hint="Örnek Ayda 1 kopya, Bütün kopyalar" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(242, 14, 14); font-style: normal; font-weight: bold;"
                                  >
                                      Örnek Ayda 1 kopya, Bütün kopyalar
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item133">
                              <div className="fb-grouplabel">
                                  <label id="item133_label_0"style={{display: 'inline'}}>Db Vault</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="DbVault" id="item133_text_1" type="text" maxLength="254" placeholder="" data-hint="Örnek 2 ay, 3 ay, 6 ay, 1 yıl, sonsuz" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(247, 19, 19); font-style: normal; font-weight: bold;"
                                >
                                      Örnek 2 ay, 3 ay, 6 ay, 1 yıl, sonsuz
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item134">
                              <div className="fb-grouplabel">
                                  <label id="item134_label_0"style={{display: 'inline'}}>Db Vault Peryodu</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text134" id="item134_text_1" type="text" maxLength="254" placeholder="" data-hint="Örnek Ayda 1 kopya, Bütün kopyalar" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(247, 17, 17); font-style: normal; font-weight: bold;"
                                  >
                                      Örnek Ayda 1 kopya, Bütün kopyalar
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item135">
                              <div className="fb-grouplabel">
                                  <label id="item135_label_0"style={{display: 'inline'}}>Vault Ek İstek</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text135" id="item135_text_1" type="text" maxLength="254" placeholder="" data-hint="X database 1 yıl" autocomplete="off" />
          
                                  <div className="fb-hint" 
                                //   style="color: rgb(247, 10, 10); font-style: normal; font-weight: bold;"
                                  >
                                      X database 1 yıl
                                  </div>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item136" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item136_label_0"style={{display: 'inline'}}>
                                      Backup İşlemlerinin gerçekleşmesi istenen zaman aralığı
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="Backuplemleriningereklemesiistenenzamanaral" id="item136_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb_cond_applied" id="item137">
                              <div className="fb-sectionbreak">
                                  <hr style={{max_width: '960px'}}/>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item138">
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>
                                                YÖNETİM GÖZETİM BİLGİLERİ
                                            </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item139">
                              <div className="fb-static-text">
                                  <p 
                                  //style="color: rgb(26, 2, 26); font-weight: bold;"
                                  >
                                      İşletime Alınacak Sunucu üzerinde gözlenmesi gereken;
                                  </p>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item140" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item140_label_0"style={{display: 'inline'}}>
                                      "a) Data ve Backup ağ arayüzleri dışında başka arayüzler var mıdır? (Evet ise lütfen belirtiniz) Varsa IP adresleri nedir? "
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="aDataveBackupaarayzleridndabakaarayzlervarmdrEvetiseltfenbelirtinizVarsaIPadreslerinedir" id="item140_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item141" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item141_label_0"style={{display: 'inline'}}>
                                      b) MS Windows sistemlerde otomatik olarak başlatılan (automatic started),Unix sistemlerde“daemon” olan &nbsp;servis/processle
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="bMSWindowssistemlerdeotomatikolarakbalatlanautomaticstartedUnixsistemlerdedaemonolannbspservisprocessle" id="item141_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item142" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item142_label_0"style={{display: 'inline'}}>
                                      c) Programlı iş (scheduled task, cron job) var mıdır? Varsa nelerdir?
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="cProgramlischeduledtaskcronjobvarmdrVarsanelerdir" id="item142_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item143" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item143_label_0"style={{display: 'inline'}}>
                                      d) URL, port gibi ağ erişim noktaları var mıdır? Varsa nelerdir?
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="dURLportgibiaeriimnoktalarvarmdrVarsanelerdir" id="item143_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item147" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item147_label_0"style={{display: 'inline'}}>
                                      Select Ping İle Gözleme Alınması İstenmekte midir?option
                                  </label>
                              </div>
                              <div className="fb-dropdown">
                                  <select name="PingleGzlemeAlnmasstenmektemidir" id="item147_select_1" required="" data-hint="">
                                      <option id="item147_0_option" value="" selected="">
                                          Lütfen Seçiniz
                                      </option>
                                      <option id="item147_1_option" value="Evet">
                                          Evet
                                      </option>
                                      <option id="item147_2_option" value="Hayır">
                                          Hayır
                                      </option>
                                  </select>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item148" style={{opacity: 1}}>
                              <div className="fb-static-text">
                                  <p 
                                //   style="color: rgb(13, 1, 13); font-weight: bold;"
                                  >
                                      İşletime Alınacak Sunucu üzerinde gözlenmemesi gereken ;
                                  </p>
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item144" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item144_label_0"style={{display: 'inline'}}>a)Ağ arayüzü var mıdır? Varsa nelerdir?</label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="aAarayzvarmdrVarsanelerdir" id="item144_text_1" required="" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column fb_cond_applied" id="item145" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item145_label_0"style={{display: 'inline'}}>
                                      b) Servis/processler/özel uygulamalar var mıdır? Varsa nelerdir?
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="bServisprocesslerzeluygulamalarvarmdrVarsanelerdir" id="item145_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item224">
                              <div className="fb-html">
                                  <div id="item224_div_0">
                                      {/* <a href="http://www.htm2pdf.co.uk/"> */}
                                      <button type="submit">
                                        Save this page as PDF
                                      </button>
                                  </div>
                              </div>
                          </div>
                          {/* <div className="fb-item fb-100-item-column" id="item186" style={{opacity: 1}}>
                              <div className="fb-header">
                                  <h2 style={{display: 'inline'}}>
                                                Alınan Hizmetlerin Özeti
                                            </h2>
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item189" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item189_label_0" 
                                //   style="color: rgb(8, 8, 8); display: none;"
                                >
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text189" disabled="" id="item189_text_1" type="text" maxLength="254" placeholder="Ms İşletim Sistemi Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item196">
                              <div className="fb-grouplabel">
                                  <label id="item196_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text196" disabled="" id="item196_text_1" type="text" maxLength="254" placeholder="Merkezi Veri Depolama Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-20-item-column fb_cond_applied" id="item197">
                              <div className="fb-grouplabel">
                                  <label id="item197_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text197" disabled="" id="item197_text_1" type="text" maxLength="254" placeholder="U Bazlı Barındırma" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-20-item-column fb_cond_applied" id="item198" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item198_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text198" disabled="" id="item198_text_1" type="text" maxLength="254" placeholder="DB2 Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-25-item-column fb_cond_applied" id="item199">
                              <div className="fb-grouplabel">
                                  <label id="item199_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text199" disabled="" id="item199_text_1" type="text" maxLength="254" placeholder="Oracle Veritabanı Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item201">
                              <div className="fb-grouplabel">
                                  <label id="item201_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text201" disabled="" id="item201_text_1" type="text" maxLength="254" placeholder="Exchange E-Posta Sunucu Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item202">
                              <div className="fb-grouplabel">
                                  <label id="item202_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text202" disabled="" id="item202_text_1" type="text" maxLength="254" placeholder="File / Printer Sunucu Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item203">
                              <div className="fb-grouplabel">
                                  <label id="item203_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text203" disabled="" id="item203_text_1" type="text" maxLength="254" placeholder="IIS Web Sunucusu Yönetim" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-20-item-column fb_cond_applied" id="item204">
                              <div className="fb-grouplabel">
                                  <label id="item204_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text204" disabled="" id="item204_text_1" type="text" maxLength="254" placeholder="MySQL Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item205">
                              <div className="fb-grouplabel">
                                  <label id="item205_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text205" disabled="" id="item205_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-50-item-column fb_cond_applied" id="item206">
                              <div className="fb-grouplabel">
                                  <label id="item206_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text206" disabled="" id="item206_text_1" type="text" maxLength="254" placeholder="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item214">
                              <div className="fb-grouplabel">
                                  <label id="item214_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text214" disabled="" id="item214_text_1" type="text" maxLength="254" placeholder="Sybase Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item215">
                              <div className="fb-grouplabel">
                                  <label id="item215_label_0" 
                                //   style="color: rgb(245, 12, 12); display: none;"
                                >
                                  </label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text215" disabled="" id="item215_text_1" type="text" maxLength="254" placeholder="Yardım Masası Hizmeti" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item195" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item195_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text195" disabled="" id="item195_text_1" type="text" maxLength="254" placeholder="Unix İşletim sistemi Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item216">
                              <div className="fb-grouplabel">
                                  <label id="item216_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text216" disabled="" id="item216_text_1" type="text" maxLength="254" placeholder="Yönetilen Sanal Veri Merkezi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item183" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item183_label_0" style={{color: 'rgb(240, 8, 8)', display: 'none'}}>></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text183" disabled="" id="item183_text_1" type="text" maxLength="254" placeholder="MS SQL Server Veritabanı Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-50-item-column fb_cond_applied" id="item200" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item200_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text200" disabled="" id="item200_text_1" type="text" maxLength="254" placeholder="Dizin Servisleri (Domain Denetçisi) Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-33-item-column fb_cond_applied" id="item187" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item187_label_0" style={{color: 'rgb(240, 8, 8)', display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text187" disabled="" id="item187_text_1" type="text" maxLength="254" placeholder="Merkezi Yedekleme Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-50-item-column fb_cond_applied" id="item207" style={{opacity: 1}}>
                              <div className="fb-grouplabel">
                                  <label id="item207_label_0" style={{display: 'none'}}></label>
                              </div>
                              <div className="fb-input-box">
                                  <input name="text207" disabled="" id="item207_text_1" type="text" maxLength="254" placeholder="SQL Server Log Shipping Yönetimi" value="" data-hint="" autocomplete="off" />
                              </div>
                          </div>
                          <div className="fb-item fb-100-item-column" id="item222" style={{opacity: 1}}>
                              <div className="fb-html">
                                  <div id="item222_div_0">
                                      <button type="reset" value="Reset">Temizle</button>
                                  </div>
                              </div>
                          </div> */}

                      </div>
                  </div>
              </form>
        //   </body>

        //   </div>
          
        );
    }
}

export default StaticForm;