import React from 'react';
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'

window.html2canvas = html2canvas

export default class DynamicForm extends React.Component{
    state = {};

    onSubmit = (e) => {
        e.preventDefault();
        // if(this.props.onSubmit){
        //     this.props.onSubmit(this.state)
        // }

        // pdf
       this.printDocument();
    }

    onChange = (e, key,type="single") => {
        console.log(`${key} changed ${e.target.value} type ${type}`);
        if (type === "single") {
            this.setState({
                [key]: e.target.value  
            });
        } else {
            this.setState({
                [key]: e.target.value  
            });
            // let found = this.state[key] ?  
            //                 this.state[key].find ((d) => d === e.target.value) : false;
            
            // if (found) {
            //     let data = this.state[key].filter((d) => {
            //         return d !== found;
            //     });
            //     this.setState({
            //         [key]: data
            //     });
            // } else {
            //     this.setState({
            //         [key]: [e.target.value, ...this.state[key]]
            //     });
            // }
        }
    }

    renderForm = () => {
        let model = this.props.model;

        let formUI = model.map((m)=>{
            let key = m.key;
            let type = m.type;
            let name = m.name;
            let value = m.value;
            let props = m.props || {};

            let target = key;  
            value = this.state[target];
            
            //{...props}
            let input =  <input {...props}
                    className="form-input"
                    type={type}
                    key={key}
                    name={name}
                    value={value}
                    onChange={(e)=>{this.onChange(e, target)}}
                />;
            
            let label = <div className="fb-grouplabel">
                            <label
                                style={{display: 'inline'}}
                                key = {"l" + key}
                                htmlFor ={m.key}>
                                    {m.label}
                            </label>
                        </div>

                if(type == 'textarea'){
                    input = <textarea {...props}
                        style = {{max_width: '960px', resize: 'none'}}
                        key = {key}
                        name = {name}
                        value = {value}
                        onChange={(e)=>{this.onChange(e, target)}}
                    >
                    </textarea>
                }
                if (type == "dropdown") {
                    input = m.options.map((o) => {
                        let checked = o.value == value;
                        // console.log("select: ", o.value, value);
                         return (
                            // {...props}
                                <option {...props}
                                    className="form-input"
                                    key={o.key}
                                    value={o.value}
                                >{o.value}</option>
                         );
                    });
    
                    // console.log("Select default: ", value);
                    input = <div className="fb-dropdown">
                                 <select value={value} onChange={(e)=>{this.onChange(e, m.key)}}>{input}</select>
                            </div>;
                   
                 }
                
                 if (type == "checkbox") {
                    input = m.options.map((o) => {
                        
                        //let checked = o.value == value;
                        let checked = false;
                        if (value && value.length > 0) {
                            checked = value.indexOf(o.value) > -1 ? true: false;
                        }
                        // console.log("Checkbox: ",checked);
                         return (
                        <React.Fragment key={"cfr" + o.key}>
                         {/* {...props} */}
                                <input {...props}
                                    className="form-input"
                                    type={type}
                                    key={o.key}
                                    name={o.name}
                                    checked={checked}
                                    value={o.value}
                                    onChange={(e)=>{this.onChange(e, m.key,"multiple")}}
                                />
                                <label key={"ll" +o.key }>{o.label}</label>
                        </React.Fragment>
                         );
                    });
    
                    input = <div className ="fb-checkbox">{input}</div>;
    
                 }

                 if (type == "radio") {
                    input = m.options.map((o) => {
                        let checked = o.value == value;
                         return (
                             <React.Fragment key={'fr' + o.key}>
                             {/* {...props} */}
                                 <input {...props}
                                         className="form-input"
                                         type={type}
                                         key={o.key}
                                         name={o.name}
                                         checked={checked}
                                         value={o.value}
                                         onChange={(e)=>{this.onChange(e, o.name)}}
                                 />
                                 <label key={"ll" +o.key }>{o.label}</label>
                             </React.Fragment>
                         );
                    });
                    input = <div className ="fb-radio">{input}</div>;
                 }
                let itemDiv = <div className="fb-item fb-100-item-column" style={{opacity: 1}}>
                                {label}
                                {input}
                             </div>;
                 return(
                        itemDiv
            );
        });
        return formUI;
    }

    printDocument = () => {
        html2canvas(document.body).then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            document.body.appendChild(canvas);
            const pdf = new jsPDF()
            pdf.addImage(imgData, 'JPEG', 15, 40, 180, 160);
            pdf.save("DynamicForm.pdf");
          });
      }


    render () {
        let title = this.props.title || "Dynamic Form";

        return (
            <div id="divToPrint">
                <h3 className="form-title">{title}</h3>
                <form className="fb-toplabel fb-100-item-column selected-object" onSubmit={(e)=>{this.onSubmit(e)}}>
                    <div className="fb-form-header" style={{min_height: '0px'}}>
                        <a className="fb-link-logo">
                            <img className="fb-logo">

                            </img>
                        </a>
                    </div>
                    <div className="section">
                        <div className="column ui-sortable">
                            {this.renderForm()}
                        </div>
                        
                    </div>
                   
                    <div className="form-actions">
                        <button type="submit">submit</button>
                    </div>
                </form>
            </div>
        )
    }
}