import React, {Component} from 'react';

class FormBuilder extends Component{

    formDataList = [];

    onSubmit = (e) => {
        e.preventDefault();
        this.generateJson();
    }

    // addDisplayRule = () => {
    //     let element = document.getElementsByClassName('selected-object');
    //     let rule = [];
    //     for(let i=0; i < element[1].children.length; i++){
    //         let currentElement =  element[1].children[i];
    //         if(currentElement.className == "fb-grouplabel"){
    //             let key = currentElement.querySelector('label').textContent;
    //            if(this.formDataList !== undefined || this.formDataList.length !== 0){
    //                 let formData = this.formDataList.find(f => f.key == key);
    //            }
    //         }
    //     }
    // }
    propsFillbyType = (type, attributes)=>{
        let prop;
        switch(type) {
            case 'input-box': //yani text
                if(attributes[7]){
                 prop = {type: attributes[0].nodeValue, 
                        id: attributes[1].nodeValue,
                        maxlenght: attributes[2].nodeValue,
                        placeholder: attributes[3].nodeValue,
                        autocomplete: attributes[4].nodeValue,
                        data_hint: attributes[5].nodeValue,
                        name: attributes[6].nodeValue,
                        required: attributes[7].nodeValue
                    }
                }
                else{
                    prop = {type: attributes[0].nodeValue, 
                        id: attributes[1].nodeValue,
                        maxlenght: attributes[2].nodeValue,
                        placeholder: attributes[3].nodeValue,
                        autocomplete: attributes[4].nodeValue,
                        data_hint: attributes[5].nodeValue,
                        name: attributes[6].nodeValue,
                    }
                }
                break;
            case 'textarea': 
                    prop = {
                        // style: attributes[0].nodeValue,
                        id: attributes[1].nodeValue,
                        maxlenght: attributes[2].nodeValue,
                        placeholder: attributes[3].nodeValue,
                        data_hint: attributes[4].nodeValue,
                        name: attributes[5].nodeValue,
                    }
                    break;
            case 'input-number':
                    prop = {
                        type: attributes[0].nodeValue,
                        id: attributes[1].nodeValue,
                        autocomplete: attributes[2].nodeValue,
                        min: attributes[3].nodeValue,
                        max: attributes[4].nodeValue,
                        step: attributes[5].nodeValue,
                        data_hint: attributes[6].nodeValue,
                        name: attributes[7].nodeValue,
                    }
                break;
            case 'dropdown':
                    prop = {
                        id: attributes[0].nodeValue,
                        data_hint: attributes[1].nodeValue,
                        name: attributes[2].nodeValue,
                    }
            case 'checkbox': 
                    prop = {
                        className: attributes[0].nodeValue
                    }
            default:
        }
        return prop;
    }
    
    fillOptions = (options) => {
        let optionList = [];
        for(let i=0; i<options.length;i++){
            let currentOption = options[i];
            let option = {
                key: currentOption.id,
                label: currentOption.label,
                value : currentOption.value
            }
            optionList.push(option);
        }
        return optionList;
    }

    fillCheckboxes = (element) => {
        let optionList = [];
        for(let i=0; i < element.children.length; i++){
            let currentCheckbox = element.children[i];
            let input = currentCheckbox.querySelector('input');

            let option = {
                key: input.id,
                label: input.value,
                value: input.value
            }
           optionList.push(option); 
        }
        return optionList;
    }
    generateJson = () =>{
        let childrenList = document.getElementById("column1").children;
        this.formDataList = [];
        NodeList.prototype.forEach = Array.prototype.forEach
        for(let i=0; i < childrenList.length; i++){
            let currentChildren = childrenList[i];
            let label;
            let key;
            let type;
            let props = [];
            let options = [];
            let rule = [];
            
            for(let i=0; i < currentChildren.children.length; i++){
                let element =  currentChildren.children[i];
                let input;
                if(element.className == "fb-grouplabel"){
                   label = element.querySelector('label').textContent;
                   key = label;

                } else{
                    type = element.className.substring(
                        element.className.indexOf("fb-") + 3,
                    );
                    switch(type){
                        case 'input-box':
                            input = element.querySelector('input'); 
                            props = this.propsFillbyType(type, input.attributes);
                            type = input.attributes[0].nodeValue;
                            break;
                        case 'textarea':
                            input = element.querySelector('textarea');
                            props = this.propsFillbyType(type, input.attributes);
                            break;
                        case 'input-number':
                            input = element.querySelector('input');
                            props = this.propsFillbyType(type, input.attributes);
                            type = input.attributes[0].nodeValue;
                            break;
                        case 'dropdown': 
                            input = element.querySelector('select');
                            props = this.propsFillbyType(type, input.attributes);
                            options = this.fillOptions(input.options);
                            break;
                        case 'checkbox':
                            props = this.propsFillbyType(type, element.attributes);
                            options = this.fillCheckboxes(element);
                            break;
                        case 'radio':
                            props = this.propsFillbyType(type, element.attributes);
                            options = this.fillCheckboxes(element);
                        default:
                    }
                    let formData = {key: key, label: label, type: type, props: props, options: options }
                    this.formDataList.push(formData);
                    console.log(JSON.stringify(this.formDataList))
                }
            }
        }
    }
    render(){
        return(
            <div>
            <div className="ui-layout-center" id="layout-center">
                <form id="docContainer" method="GET" action="" novalidate="novalidate" onSubmit={(e) => {this.onSubmit(e)}}> 
                {/* enctype="multipart/form-data" */}
                <button type="submit">Test</button>
                <button onClick={(e) => this.addDisplayRule()}>Apply Rule</button>
                </form>
            </div>
            <div className="ui-layout-east" id="controls">
              <div id="control_toolbar">
                <a href="#elements" id="elements">Elements</a>
                <a href="#objectproperties" id="objectproperties">Properties</a>
                <a href="#formproperties" id="formproperties">Form Options</a>
            </div>
              <form id="controlset" className="tabsform" method="get" action="">
                <div id="tb_form_tools" className="controlpane">
                  <fieldset id="tool_group_basic" className="tool_group">
                    <legend>Basic</legend>
                    <div id="tool_heading" className="drag_tool" title="Click or drag to insert a Title/Heading"><span>Title/Heading</span></div>
                    <div id="tool_text" className="drag_tool" title="Click or drag to insert Plain Text"><span>Plain Text</span></div>
                    <div id="tool_textfield" className="drag_tool" title="Click or drag to insert a Text Field"><span>Text Field</span></div>
                    <div id="tool_textarea" className="drag_tool" title="Click or drag to insert a Text Area"><span>Text Area</span></div>
                    <div id="tool_number" className="drag_tool" title="Click or drag to insert a Number field"><span>Number</span></div>
                    <div id="tool_password" className="drag_tool" title="Click or drag to insert a Password field"><span>Password</span></div>
                    <div id="tool_dropdown" className="drag_tool" title="Click or drag to insert a Drop Down menu"><span>Drop Down</span></div>
                    <div id="tool_radio" className="drag_tool" title="Click or drag to insert Radio Buttons"><span>Radio Buttons</span></div>
                    <div id="tool_checkbox" className="drag_tool" title="Click or drag to insert Checkboxes"><span>Checkboxes</span></div>
                    <div id="tool_upload" className="drag_tool" title="Click or drag to insert a File Upload button"><span>Upload File</span></div>		  
                  </fieldset>
                  <fieldset id="tool_group_magic" className="tool_group">
                    <legend>Magical</legend>
                    <div id="tool_email" className="drag_tool" title="Click or drag to insert an Email Address field"><span>Email Address</span></div>
                    <div id="tool_url" className="drag_tool" title="Click or drag to insert a Web Address field"><span>Web Address</span></div>
                    <div id="tool_date" className="drag_tool" title="Click or drag to insert a Date selector"><span>Date</span></div>
                    <div id="tool_listbox" className="drag_tool" title="Click or drag to insert a List Box"><span>List Box</span></div>
                    <div id="tool_image" className="drag_tool" title="Click or drag to insert an Image"><span>Image</span></div>
                    <div id="tool_phone" className="drag_tool" title="Click or drag to insert a Phone Number"><span>Phone Number</span></div>
                    <div id="tool_sectionbreak" className="drag_tool" title="Click or drag to insert a Section Break"><span>Section Break</span></div>
                    <div id="tool_spacer" className="drag_tool" title="Click or drag to insert a Spacer"><span>Spacer</span></div>
                       <div id="tool_regex" className="drag_tool" title="Click or drag to insert a Regular Expression Manager"><span>Reg. Expression</span></div>
                    <div id="tool_html" className="drag_tool" title="Click or drag to insert HTML"><span>HTML</span></div>
                  </fieldset>
                  <fieldset id="tool_group_static" className="tool_group">
                    <legend>Static</legend>
                    <div id="tool_header"  className="controls_static" title="Click to show Header properties"><span>Header</span></div>
                    <div id="tool_footer"  className="controls_static" title="Click to show Footer properties"><span>Footer</span></div>   
                    <div id="tool_logo"  className="controls_static" title="Click to show Logo properties"><span>Logo</span></div>
                    <div id="tool_submit" type="submit" className="controls_static" title="Click to show Submit Button properties"><span>Submit</span></div>
                    <div id="tool_captcha"  className="controls_static" title="Click to show Captcha properties"><span>Captcha</span></div>
                    <div id="tool_sigpad"  className="controls_static" title="Click to show Signature properties"><span>Signature</span></div>
                  </fieldset>
                  <fieldset id="tool_workflow_switcher" style={{display:'none'}}> 
                  {/* style="display:none" */}
                    <legend>Workflow</legend>
                    <label><input type="checkbox" id="workflow_switcher"/><span>  Show properties after insertion</span></label>
                  </fieldset>
                  <hr className="tool_group_divider"/>
                  <div id="tool_item_duplicate" className="action_tool" title="Duplicate the selected element">Duplicate</div>
                  <div id="tool_item_delete" className="action_tool" title="Delete the selected element">Delete</div>
                </div>
                <div id="tb_control_properties" className="controlpane"><fieldset id="control_properties_set"><p id="properties_placeholder">Select an element and its editable properties will appear here.</p></fieldset></div>
                <div id="tb_form_properties" className="controlpane"></div>
              </form>
            </div>
            </div>
        );
    }
}

export default FormBuilder;