import React from 'react';
import ReactDOM from 'react-dom';
import './theme/index.css';
import './theme/default.css';
// import App from './App';
// import FormBuilder from './components/builder/formBuilder'
import StaticForm from './components/test-component/static-form'
// import Form from './components/test-component/form';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<StaticForm/>, document.getElementById('root'));
registerServiceWorker();
